<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/main'); ?>

	<div class="container">
		<?php echo $content; ?>
	</div>
	
<?php $this->endContent(); ?>