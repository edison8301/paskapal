<a href="#" class="logo">
    <!-- LOGO -->
    <span class="logo-lg">PAS KAPAL</span>
</a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/resources/dist/img/default-avatar.png" class="user-image" alt="User Image">
            <span class="hidden-xs"><?php echo Yii::app()->user->id ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?php echo Yii::app()->theme->baseUrl; ?>/resources/dist/img/default-avatar.png" class="img-circle" alt="User Image">
              <p>
                <?php echo Yii::app()->user->id ?>
                <!-- <small>Member since Nov. 2012</small> -->
              </p>
            </li>

            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi dolore eius qui at est accusamus architecto corporis aut eveniet repellendus non deserunt fugit eaque, neque soluta, fuga eos nisi quidem. Deleniti reprehenderit harum excepturi perspiciatis, tenetur consequatur provident qui aspernatur dolores laborum. Libero ab illum beatae ratione laboriosam. Voluptas error, eos tenetur velit, ipsam, blanditiis tempore sed assumenda atque recusandae voluptatem distinctio eligendi ipsum repellat asperiores quidem magni fuga magnam commodi totam minus sint accusamus? Modi a repudiandae deleniti libero eum, sapiente blanditiis! Quidem, corporis quaerat consequuntur fuga cum! Velit nisi quis quos enim odio officia ullam amet est debitis.
                <a href="<?php echo Yii::app()->createUrl('instansi/profile')?>" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="<?php echo Yii::app()->createUrl('site/logout')?>" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
