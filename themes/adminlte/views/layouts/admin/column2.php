<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/main'); ?>


<div class="content-header">
    <h1><?php echo $this->pageTitle; ?><small></small></h1>
    
    <!-- Breadcumb-->
        <?php if(isset($this->breadcrumbs)):
            if ( Yii::app()->controller->route !== '?r=/site/index' )
            $this->breadcrumbs = array_merge(array (Yii::t('zii','<i class="fa fa-dashboard"></i>')=>Yii::app()->homeUrl.'?r=/site/index'), $this->breadcrumbs);

            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
                'homeLink'=>false,
                'encodeLabel'=>false,
                'tagName'=>'ul',
                'separator'=>'',
                'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
                'htmlOptions'=>array ('class'=>'breadcrumb')
            ));
            //<!-- breadcrumbs -->
    endif ?>

    <?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
                    echo '<div class="alert alert-' . $key . '" style="margin-top:10px;margin-bottom:0px">';
                    echo '<button type="button" class="close" data-dismiss="alert">x</button>';
                    print $message;
                    print "</div>\n";
    } ?>
</div>

<!-- Main content -->
<section class="content">
    <?php echo $content; ?>
</section>


<?php $this->endContent(); ?>
