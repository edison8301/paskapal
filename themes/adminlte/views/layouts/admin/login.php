<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="author" content="Digital Data Studio">
    <meta name="robots" content="noodp,noydir"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php
      $baseUrl = Yii::app()->theme->baseUrl;
      $resources = Yii::app()->theme->baseUrl.'/resources'; 
    ?>
    <!-- Bootstrap 3.3.4 -->
    <!-- <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
   
    <!-- Font Awesome Icons -->
    <!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo $resources;?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Theme style -->
    <link href="<?php echo $resources;?>/dist/css/adminlte.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo $resources;?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    
    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl;?>/css/login.css" rel="stylesheet" type="text/css" />
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
      <?php echo $content; ?>
  </div>
    
    <!-- jQuery 2.1.4 -->
    <!--<script src="<?php //echo $resources;?>/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>-->
    <!-- Bootstrap 3.3.2 JS -->
    <!--<script src="<?php //echo $resources;?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->
    <!-- iCheck -->
    <script src="<?php echo $resources;?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>