<!-- Inner sidebar -->
  <div class="sidebar" style="margin-bottom: 500px">
	<!-- user panel (Optional) -->
	<div class="user-panel">
		<div class="pull-left image">
			<img src="<?= Yii::app()->theme->baseUrl.'/resources' ?>/dist/img/avatar.png" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p><?php echo Yii::app()->user->id ?></p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div><!-- /.user-panel -->

	<!-- Sidebar Menu -->
	<ul class="sidebar-menu">
		<li class="header">MENU ADMIN</li>
		<li><a href="<?php echo Yii::app()->createUrl('site/index');?>"><i class="fa fa-list"></i> <span>Dashboard</span></a></li>
		<li><a href="<?php echo Yii::app()->createUrl('pasKecil/admin');?>"><i class="fa fa-tags"></i> <span>Pas Kecil</span></a></li>
		<li><a href="<?php echo Yii::app()->createUrl('pasKecil/export');?>"><i class="fa fa-tags"></i> <span>Export Pas Kecil</span></a></li>
		<li><a href="<?php echo Yii::app()->createUrl('retribusi/admin');?>"><i class="fa fa-tags"></i> <span>Retribusi</span></a></li>
		<li><a href="<?php echo Yii::app()->createUrl('retribusi/export');?>"><i class="fa fa-tags"></i> <span>Export Retribusi</span></a></li>
		<li><a href="<?php echo Yii::app()->createUrl('user/admin');?>"><i class="fa fa-user"></i> <span>User</span></a></li>
		<li><a href="<?php echo Yii::app()->createUrl('pengaturan/admin');?>"><i class="fa fa-wrench"></i> <span>Pengaturan</span></a></li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-database"></i>
				<span>Master Data</span>
			</a>
			<ul class="treeview-menu">
					<li><?= CHtml::link('<i class="fa fa-tags"></i> Jenis Kapal',['jenisKapal/admin']); ?></li>
					<li><?= CHtml::link('<i class="fa fa-tags"></i> Bahan Utama',['bahanUtama/admin']); ?></li>
			</ul>
		</li>
		<li><a href="<?php echo Yii::app()->createUrl('site/logout');?>"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>

	</ul><!-- /.sidebar-menu -->
</div><!-- /.sidebar -->
