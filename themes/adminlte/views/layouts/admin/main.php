<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/resources/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/resources/dist/css/adminlte.min.css" />

    <!-- Skin -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/resources/dist/css/skins/_all-skins.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/resources/plugins/ionicons/css/ionicons.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/resources/plugins/datatables/dataTables.bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/admin.css" />
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/custom.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body {
            min-height: 100%;
            padding: 0;
            margin: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
    </style>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/vendors/floatThead/dist/jquery.floatThead.min.js'); ?>


</head>
<body class="skin-blue sidebar-mini">


<div class="wrapper">
    <header class="main-header">
        <?php include_once '_header.php'; ?>
    </header>

    <aside class="main-sidebar">
        <?php include_once '_sidebar.php'; ?>
    </aside>

    <div class="content-wrapper">
        <?php echo $content ?>
    </div>




</div><!-- wrapper -->



    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/resources/dist/js/app.min.js"></script>

</body>
</html>
