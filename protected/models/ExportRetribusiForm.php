<?php

class ExportRetribusiForm extends CFormModel
{
    public $berdasarkan_tanggal;
    public $tanggal_awal;
    public $tanggal_akhir;

    public function rules()
    {
        return array(
            array('berdasarkan_tanggal, tanggal_awal, tanggal_akhir', 'required','message'=>'{attribute} tidak boleh kosong'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'berdasarkan_tanggal'=>'Export Berdasarkan',
            'tanggal_awal'=>'Tanggal Awal',
            'tanggal_akhir'=>'Tanggal Akhir',
        );
    }
}