<?php

/**
 * This is the model class for table "pas_kecil".
 *
 * The followings are the available columns in table 'pas_kecil':
 * @property integer $id
 * @property integer $id_kapal
 * @property string $nomor
 * @property string $nama_kapal
 * @property string $nama_pemilik
 * @property string $alamat_pemilik
 * @property string $tanda_pas
 * @property string $gross_tonnage
 * @property string $panjang_kapal
 * @property string $lebar_kapal
 * @property string $dalam_kapal
 * @property string $mesin_penggerak
 * @property integer $bahan_utama
 * @property integer $jenis_kapal
 * @property integer $tahun_pembuatan_kapal
 * @property string $tempat_register_kapal
 * @property string $nomor_register_kapal
 * @property string $catatan
 * @property string $tujuan_penggunaan
 * @property string $tanggal_pemberian
 * @property string $tempat_pemberian
 * @property string $tanggal_batas_berlaku
 */
class PasKecil extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pas_kecil';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kapal, bahan_utama, jenis_kapal, tahun_pembuatan_kapal, jumlah_geladak, jumlah_baling_baling', 'numerical', 'integerOnly'=>true),
			array('nomor, nama_kapal, nama_pemilik, alamat_pemilik, tanda_pas,tempat_pembuatan_kapal, gross_tonnage, net_tonnage, mesin_penggerak, merk_mesin, daya_mesin, tempat_register_kapal, nomor_register_kapal, catatan, tujuan_penggunaan, tempat_pemberian', 'length', 'max'=>255),
			array('panjang_kapal, lebar_kapal, dalam_kapal', 'length', 'max'=>20),
			array('tanggal_pemberian, tanggal_batas_berlaku', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_kapal, nomor, nama_kapal, nama_pemilik, alamat_pemilik, tanda_pas, gross_tonnage, panjang_kapal, lebar_kapal, dalam_kapal, mesin_penggerak, bahan_utama, mesin_penggerak, merk_mesin, jenis_kapal, tahun_pembuatan_kapal, tempat_register_kapal, nomor_register_kapal, catatan, tujuan_penggunaan, tanggal_pemberian, tempat_pemberian, tanggal_batas_berlaku', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_kapal' => 'Id Kapal',
			'nomor' => 'Nomor',
			'nama_kapal' => 'Nama Kapal',
			'nama_pemilik' => 'Nama Pemilik',
			'alamat_pemilik' => 'Alamat Pemilik',
			'tanda_pas' => 'Tanda Pas',
			'gross_tonnage' => 'Gross Tonnage',
			'net_tonnage' => 'Net Tonnage',
			'panjang_kapal' => 'Panjang Kapal',
			'lebar_kapal' => 'Lebar Kapal',
			'dalam_kapal' => 'Dalam Kapal',
			'mesin_penggerak' => 'Mesin Penggerak',
			'merk_mesin' => 'Merk Mesin',
			'daya_mesin' => 'Daya Mesin',
			'bahan_utama' => 'Bahan Utama',
			'jumlah_geladak' => 'Jumlah geladak',
			'jumlah_baling_baling' => 'Jumlah Baling-baling',
			'jenis_kapal' => 'Jenis Kapal',
			'tempat_pembuatan_kapal' => 'Tempat Pembuatan Kapal',
			'tahun_pembuatan_kapal' => 'Tahun Pembuatan Kapal',
			'tempat_register_kapal' => 'Tempat Register Kapal',
			'nomor_register_kapal' => 'Nomor Register Kapal',
			'catatan' => 'Catatan',
			'tujuan_penggunaan' => 'Tujuan Penggunaan',
			'tanggal_pemberian' => 'Tanggal Pemberian',
			'tempat_pemberian' => 'Tempat Pemberian',
			'tanggal_batas_berlaku' => 'Tanggal Batas Berlaku',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_kapal',$this->id_kapal);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('nama_kapal',$this->nama_kapal,true);
		$criteria->compare('nama_pemilik',$this->nama_pemilik,true);
		$criteria->compare('alamat_pemilik',$this->alamat_pemilik,true);
		$criteria->compare('tanda_pas',$this->tanda_pas,true);
		$criteria->compare('gross_tonnage',$this->gross_tonnage,true);
		$criteria->compare('net_tonnage',$this->net_tonnage,true);
		$criteria->compare('panjang_kapal',$this->panjang_kapal,true);
		$criteria->compare('lebar_kapal',$this->lebar_kapal,true);
		$criteria->compare('dalam_kapal',$this->dalam_kapal,true);
		$criteria->compare('mesin_penggerak',$this->mesin_penggerak,true);
		$criteria->compare('merk_mesin',$this->merk_mesin,true);
		$criteria->compare('daya_mesin',$this->daya_mesin,true);
		$criteria->compare('bahan_utama',$this->bahan_utama,true);
		$criteria->compare('jumlah_geladak',$this->jumlah_geladak,true);
		$criteria->compare('jumlah_baling_baling',$this->jumlah_baling_baling,true);
		$criteria->compare('jenis_kapal',$this->jenis_kapal,true);
		$criteria->compare('tempat_pembuatan_kapal',$this->tempat_pembuatan_kapal,true);
		$criteria->compare('tahun_pembuatan_kapal',$this->tahun_pembuatan_kapal,true);
		$criteria->compare('tempat_register_kapal',$this->tempat_register_kapal,true);
		$criteria->compare('nomor_register_kapal',$this->nomor_register_kapal,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('tujuan_penggunaan',$this->tujuan_penggunaan,true);
		$criteria->compare('tanggal_pemberian',$this->tanggal_pemberian,true);
		$criteria->compare('tempat_pemberian',$this->tempat_pemberian,true);
		$criteria->compare('tanggal_batas_berlaku',$this->tanggal_batas_berlaku,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PasKecil the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getBahanUtama($id)
	{
		$data = BahanUtama::model()->findByPk($id);
		if ($data) {
			return $data->nama;
		}
		else
		{
			return 'N/A';
		}
	}

	public static function getJenisKapal($id)
	{
		$data = JenisKapal::model()->findByPk($id);
		if ($data) {
			return $data->nama;
		}
		else
		{
			return 'N/A';
		}
	}

	public static function countPasKapalHariIni()
	{
		$criteria = new CDbCriteria;
		$params = array();
		$criteria->addCondition('tanggal_pemberian = :tanggal_hari_ini');
		$params[':tanggal_hari_ini'] = date('Y-m-d');
		$criteria->params = $params;

		$data = PasKecil::model()->findAll($criteria);
		$jumlah = count($data);

		return $jumlah;
				
	}

	public static function countPasKapalMingguIni()
	{
		$criteria = new CDbCriteria;
		$params = array();
		$criteria->addCondition('tanggal_pemberian >= :tanggal_awal');
		$params[':tanggal_awal'] = date('Y-m-d', strtotime("monday this week"));
		$criteria->addCondition('tanggal_pemberian <= :tanggal_akhir');
		$params[':tanggal_akhir'] = date('Y-m-d', strtotime("sunday this week"));
		$criteria->params = $params;

		$data = PasKecil::model()->findAll($criteria);
		$jumlah = count($data);

		return $jumlah;
				
	}

	public static function countPasKapalBulanIni()
	{
		$criteria = new CDbCriteria;
		$params = array();
		$criteria->addCondition('tanggal_pemberian >= :tanggal_awal');
		$params[':tanggal_awal'] = date('Y-m-01');
		$criteria->addCondition('tanggal_pemberian <= :tanggal_akhir');
		$params[':tanggal_akhir'] = date('Y-m-t');
		$criteria->params = $params;

		$data = PasKecil::model()->findAll($criteria);
		$jumlah = count($data);

		return $jumlah;
				
	}

	public static function countPasKapalTahunIni()
	{
		$criteria = new CDbCriteria;
		$params = array();
		$criteria->addCondition('tanggal_pemberian >= :tanggal_awal');
		$params[':tanggal_awal'] = date('Y-1-d');
		$criteria->addCondition('tanggal_pemberian <= :tanggal_akhir');
		$params[':tanggal_akhir'] = date('Y-12-d');
		$criteria->params = $params;

		$data = PasKecil::model()->findAll($criteria);
		$jumlah = count($data);

		return $jumlah;
				
	}

	public static function getGraphListPendaftaran()
	{
		$chart = '';

		for($i=1;$i<=date('n');$i++)
		{

   			$dataChartBulan = '';

		for($i=1;$i<=12;$i++)
		{
   			$criteria = new CDbCriteria;
    
    		$bulan = $i;

    		if($i<=10) $bulan = '0'.$i;
   			$awal = date('Y').'-'.$bulan.'-01';
    		$akhir = date('Y').'-'.$bulan.'-31';
    
		    $criteria->condition = 'tanggal_pemberian >= :awal AND tanggal_pemberian <= :akhir';
		    $criteria->params = array(':awal'=>$awal,':akhir'=>$akhir);

		    $count = PasKecil::model()->count($criteria);

		    $nama_bulan = '';
		    if($i==1) $nama_bulan = 'Jan';
		    if($i==2) $nama_bulan = 'Feb';
		    if($i==3) $nama_bulan = 'Mar';
		    if($i==4) $nama_bulan = 'Apr';
		    if($i==5) $nama_bulan = 'Mei';
		    if($i==6) $nama_bulan = 'Jun';
		    if($i==7) $nama_bulan = 'Jul';
		    if($i==8) $nama_bulan = 'Aug';
		    if($i==9) $nama_bulan = 'Sep';
		    if($i==10) $nama_bulan = 'Oct';
		    if($i==11) $nama_bulan = 'Nov';
		    if($i==12) $nama_bulan = 'Des';
    
   			$dataChartBulan .= '{"label":"'.$nama_bulan.'","value":"'.$count.'"},';
		}
		return $dataChartBulan;
	}
	}

	public static function getGraphListJenisKapal()
	{
		$dataChart = '';

		$criteria = new CDbCriteria;
		$criteria->group = 'jenis_kapal';
		$criteria->order = 'jenis_kapal';
		foreach (PasKecil::model()->findAll($criteria) as $data) {
			$jenis_kapal = PasKecil::model()->getJenisKapal($data->jenis_kapal);
			$jumlah = PasKecil::model()->countByAttributes(array('jenis_kapal'=>$data->jenis_kapal));

			$dataChart .= '{"label":"'.$jenis_kapal.'","value":"'.$jumlah.'"},';
		}
		return $dataChart;
	}

	public static function getGraphListBahanUtama()
	{
		$dataChart = '';

		$criteria = new CDbCriteria;
		$criteria->group = 'bahan_utama';
		$criteria->order = 'bahan_utama';
		foreach (PasKecil::model()->findAll($criteria) as $data) {
			$bahan_utama = PasKecil::model()->getBahanUtama($data->bahan_utama);
			$jumlah = PasKecil::model()->countByAttributes(array('bahan_utama'=>$data->bahan_utama));

			$dataChart .= '{"label":"'.$bahan_utama.'","value":"'.$jumlah.'"},';
		}
		return $dataChart;
	}

	public static function getGraphListMesinPenggerak()
	{
		$dataChart = '';

		$criteria = new CDbCriteria;
		$criteria->group = 'mesin_penggerak';
		$criteria->order = 'mesin_penggerak';
		foreach (PasKecil::model()->findAll($criteria) as $data) {
			if ($data->mesin_penggerak == null || $data->mesin_penggerak == '') 
				$mesin_penggerak = 'N/A';
			else
				$mesin_penggerak = $data->mesin_penggerak;

			$jumlah = PasKecil::model()->countByAttributes(array('mesin_penggerak'=>$data->mesin_penggerak));

			$dataChart .= '{"label":"'.$mesin_penggerak.'","value":"'.$jumlah.'"},';
		}
		return $dataChart;
	}
}
