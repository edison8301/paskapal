<?php

/**
 * This is the model class for table "kapal".
 *
 * The followings are the available columns in table 'kapal':
 * @property integer $id
 * @property string $nama_kapal
 * @property string $tanda_pas
 * @property string $nama_pemilik
 * @property string $alamat_pemilik
 * @property string $npwp_pemilik
 * @property string $ukuran_p
 * @property string $ukuran_l
 * @property string $ukuran_d
 * @property string $tonase_kotor
 * @property string $tonase_bersih
 * @property string $tempat_pembangunan
 * @property string $tahun_pembangunan
 * @property string $bahan_utama
 * @property integer $jumlah_geladak
 * @property string $penggerak
 * @property string $mesin_merek
 * @property string $mesin_daya
 */
class Kapal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kapal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kapal, tanda_pas, nama_pemilik, alamat_pemilik, npwp_pemilik, ukuran_p, ukuran_l, ukuran_d, tonase_kotor, tonase_bersih, tempat_pembangunan, tahun_pembangunan, bahan_utama, jumlah_geladak, penggerak, mesin_merek, mesin_daya', 'required'),
			array('jumlah_geladak', 'numerical', 'integerOnly'=>true),
			array('nama_kapal, tanda_pas, bahan_utama, penggerak, mesin_merek', 'length', 'max'=>200),
			array('nama_pemilik, alamat_pemilik, npwp_pemilik, tempat_pembangunan, mesin_daya', 'length', 'max'=>255),
			array('ukuran_p, ukuran_l, ukuran_d, tonase_kotor, tonase_bersih', 'length', 'max'=>5),
			array('tahun_pembangunan', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_kapal, tanda_pas, nama_pemilik, alamat_pemilik, npwp_pemilik, ukuran_p, ukuran_l, ukuran_d, tonase_kotor, tonase_bersih, tempat_pembangunan, tahun_pembangunan, bahan_utama, jumlah_geladak, penggerak, mesin_merek, mesin_daya', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_kapal' => 'Nama Kapal',
			'tanda_pas' => 'Tanda Pas',
			'nama_pemilik' => 'Nama Pemilik',
			'alamat_pemilik' => 'Alamat Pemilik',
			'npwp_pemilik' => 'Npwp Pemilik',
			'ukuran_p' => 'Ukuran P',
			'ukuran_l' => 'Ukuran L',
			'ukuran_d' => 'Ukuran D',
			'tonase_kotor' => 'Tonase Kotor',
			'tonase_bersih' => 'Tonase Bersih',
			'tempat_pembangunan' => 'Tempat Pembangunan',
			'tahun_pembangunan' => 'Tahun Pembangunan',
			'bahan_utama' => 'Bahan Utama',
			'jumlah_geladak' => 'Jumlah Geladak',
			'penggerak' => 'Penggerak',
			'mesin_merek' => 'Mesin Merek',
			'mesin_daya' => 'Mesin Daya',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_kapal',$this->nama_kapal,true);
		$criteria->compare('tanda_pas',$this->tanda_pas,true);
		$criteria->compare('nama_pemilik',$this->nama_pemilik,true);
		$criteria->compare('alamat_pemilik',$this->alamat_pemilik,true);
		$criteria->compare('npwp_pemilik',$this->npwp_pemilik,true);
		$criteria->compare('ukuran_p',$this->ukuran_p,true);
		$criteria->compare('ukuran_l',$this->ukuran_l,true);
		$criteria->compare('ukuran_d',$this->ukuran_d,true);
		$criteria->compare('tonase_kotor',$this->tonase_kotor,true);
		$criteria->compare('tonase_bersih',$this->tonase_bersih,true);
		$criteria->compare('tempat_pembangunan',$this->tempat_pembangunan,true);
		$criteria->compare('tahun_pembangunan',$this->tahun_pembangunan,true);
		$criteria->compare('bahan_utama',$this->bahan_utama,true);
		$criteria->compare('jumlah_geladak',$this->jumlah_geladak);
		$criteria->compare('penggerak',$this->penggerak,true);
		$criteria->compare('mesin_merek',$this->mesin_merek,true);
		$criteria->compare('mesin_daya',$this->mesin_daya,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kapal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getSelect2List()
	{
		$list = array();
		foreach(Kapal::model()->findAll() as $data)
		{
			$list[] = $data->nama_kapal.' - '.$data->nama_pemilik;
		}

		return $list;
	}
}
