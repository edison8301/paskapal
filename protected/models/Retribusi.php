<?php

/**
 * This is the model class for table "retribusi".
 *
 * The followings are the available columns in table 'retribusi':
 * @property integer $id
 * @property string $nama
 * @property integer $no_urut
 * @property string $alamat
 * @property string $npwr
 * @property string $uraian_retribusi
 * @property integer $jumlah_sanksi_bunga
 * @property integer $jumlah_sanksi_kenaikan
 * @property integer $jumlah_keseluruhan
 * @property string $gross_tonnage
 */
class Retribusi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'retribusi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.j
		return array(
			array('', 'required'),
			array('no_urut', 'numerical', 'integerOnly'=>true),
			array('nama, npwr, alamat, tanggal_jatuh_tempo, tanggal_retribusi, masa_berlaku_uji, sampai_dengan_tanggal, uraian_retribusi', 'length', 'max'=>255),
			array('jumlah_sanksi_bunga, jumlah_sanksi_kenaikan, jumlah_keseluruhan, jumlah_ketetapan', 'length', 'max'=>20),
			array('gross_tonnage', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, no_urut, alamat, npwr, tanggal_jatuh_tempo, masa_berlaku_uji, sampai_dengan_tanggal, uraian_retribusi, jumlah_sanksi_bunga, jumlah_sanksi_kenaikan, jumlah_keseluruhan, gross_tonnage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'no_urut' => 'No Urut',
			'alamat' => 'Alamat',
			'npwr' => 'NPWR',
			'tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
			'tanggal_retribusi' => 'Tanggal Retribusi',
			'masa_berlaku_uji' => 'Masa Berlaku Uji',
			'sampai_dengan_tanggal' => 'Sampai Dengan Tanggal',
			'uraian_retribusi' => 'Uraian Retribusi',
			'jumlah_ketetapan' => 'Jumlah Ketetapan Retribusi',
			'jumlah_sanksi_bunga' => 'Jumlah Sanksi Bunga',
			'jumlah_sanksi_kenaikan' => 'Jumlah Sanksi Kenaikan',
			'jumlah_keseluruhan' => 'Jumlah Keseluruhan',
			'gross_tonnage' => 'Gross Tonnage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('no_urut',$this->no_urut);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('npwr',$this->npwr,true);
		$criteria->compare('tanggal_jatuh_tempo',$this->tanggal_jatuh_tempo,true);
		$criteria->compare('tanggal_retribusi',$this->tanggal_retribusi,true);
		$criteria->compare('masa_berlaku_uji',$this->masa_berlaku_uji,true);
		$criteria->compare('sampai_dengan_tanggal',$this->sampai_dengan_tanggal,true);
		$criteria->compare('uraian_retribusi',$this->uraian_retribusi,true);
		$criteria->compare('jumlah_ketetapan',$this->jumlah_ketetapan,true);
		$criteria->compare('jumlah_sanksi_bunga',$this->jumlah_sanksi_bunga);
		$criteria->compare('jumlah_sanksi_kenaikan',$this->jumlah_sanksi_kenaikan);
		$criteria->compare('jumlah_keseluruhan',$this->jumlah_keseluruhan);
		$criteria->compare('gross_tonnage',$this->gross_tonnage,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Retribusi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
