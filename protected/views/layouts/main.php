<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<script src="<?php print Yii::app()->request->baseUrl; ?>/vendors/accounting/accounting.min.js" type="text/javascript"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="header">
	
		<img class="logo" src="<?php print Yii::app()->baseUrl; ?>/images/logopaskapal.png">
	
</div>

<div id="adminnav">
		<?php $this->widget('booster.widgets.TbNavbar',array(
			'brand' => '',
			'fixed' => false,
			'fluid' => true,
			'type'=>'',
			'items'=>array(
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'items' => array(
						array('label'=>'Dashboard','icon'=>'home', 'url'=>array('/site/index')),
						array('label' => 'Ganti Password','icon'=>'lock', 'url' => array('user/changePassword')),
						array('label'=>'Masuk', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Keluar ('.Yii::app()->user->name.')','icon'=>'off', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

						)
					)
			),
		)); ?>
</div><!-- adminnav -->

<div class="containers" id="page">
	<div class="row">
		<div class="col-lg-12">
				<?php echo $content; ?>
		</div>
	</div>
	
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> Dinas Perhubungan Kabupaten Belitung Timur
	</div><!-- footer -->
</div><!-- page -->
<script>
    $(document).ready(function(){
    
    $(".format-money").on("keyup", function(){
        var _this = $(this);
        var value = _this.val().replace(/\.| /g,"");
        _this.val(accounting.formatMoney(value, "", 0, ".", ","))
    });

});

</script>
</body>
</html>
