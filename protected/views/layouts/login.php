<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/login.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div id="content">
	<div class="container">
		<div class="row">
			<div style="width:380px;margin-left:auto;margin-right:auto;margin-top:150px">
			<?php echo $content; ?>
			</div>
			<?php /*
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">&nbsp;</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">		
					
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">&nbsp;</div>
			*/ ?>
		</div>
	</div>
</div>

</body>
</html>
