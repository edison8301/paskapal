<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row" id="leftMenu" >
	<div class="col-sm-2" >
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'htmlOptions' => array('class' => 'left-menu-style'),
			'items' => array(
				array('label' => 'Dashboard','icon'=>'home', 'url' => array('site/index')),
				array('label'=>'Pas Kecil','icon'=>'list', 'url'=>array('/pasKecil/admin')),
				array('label'=>'Export Pas Kecil','icon'=>'download-alt', 'url'=>array('/pasKecil/export')),
				/*array('label'=>'Kapal','icon'=>'plus', 'url'=>array('/kapal/admin')),*/
				array('label'=>'Retribusi','icon'=>'tags', 'url'=>array('/retribusi/admin')),
				array('label'=>'Export Retribusi','icon'=>'download-alt', 'url'=>array('/retribusi/export')),
				array('label'=>'User','icon'=>'user', 'url'=>array('/user/admin'),'visible'=>Yii::app()->user->isAdmin()),
				array('label'=>'Pengaturan','icon'=>'wrench', 'url'=>array('/pengaturan/admin')),
				array('label'=>'Master Data','icon'=>'asterisk', 'items'=>array(
			            	array('label'=>'Jenis Kapal','icon'=>'th-large', 'url'=>array('/jenisKapal/admin')),
			                array('label'=>'Bahan Utama','icon'=>'unchecked','url'=>array('/bahanUtama/admin')),
			                )),
				array('label'=>'Logout ('.Yii::app()->user->name.')','icon'=>'off','url'=>array('site/logout'))
			)
	)); ?>
	</div>


	<div class="col-sm-10">
		<div class="row">
    		<div class="col-sm-12">
    			<div id="breadcumbs">
            		<?php if(isset($this->breadcrumbs)) {
                    	if ( Yii::app()->controller->route !== 'site/index' )

                        $this->breadcrumbs = array_merge(array (Yii::t('zii','<i class="glyphicon glyphicon-home"></i>')=>Yii::app()->homeUrl.'?r=site/index'), $this->breadcrumbs);
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                                'links'=>$this->breadcrumbs,
                                'htmlOptions' => array('class'=>'dim'),
                                'homeLink'=>false,
                                'encodeLabel'=>false,
                                'htmlOptions'=>array ('class'=>'breadcrumb')
                        ));
            		} ?>
            	</div>
            </div>
    	</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="box-content">
				<?php
					foreach(Yii::app()->user->getFlashes() as $key => $message) {
						echo '<div class="alert alert-' . $key . '">';
						echo '<button type="button" class="close" data-dismiss="alert">x</button>';
						print $message;
						print "</div>\n";	
					}
				?>
				</div>
			<?php echo $content; ?>
			</div>
		</div>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>