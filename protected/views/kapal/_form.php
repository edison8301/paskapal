<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'kapal-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<div class="well">

	<?php echo $form->textFieldGroup($model,'nama_kapal',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array()
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'tanda_pas',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'class'=>'span5',
					'maxlength'=>200)
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'nama_pemilik',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'class'=>'span5',
					'maxlength'=>255)
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'alamat_pemilik',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'class'=>'span5',
					'maxlength'=>255)
					)
	)); ?>

	<?php echo $form->textFieldGroup($model,'npwp_pemilik',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>255)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'ukuran_p',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>5)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'ukuran_l',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>5)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'ukuran_d',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>5)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'tonase_kotor',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>5)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'tonase_bersih',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>5)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'tempat_pembangunan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>255)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'tahun_pembangunan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>4)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'bahan_utama',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>200)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'jumlah_geladak',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5')
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'penggerak',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array('htmlOptions'=>array(
	'class'=>'span5','maxlength'=>200)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'mesin_merek',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>200)
	)
	)); ?>

	<?php echo $form->textFieldGroup($model,'mesin_daya',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>array(
	'htmlOptions'=>array(
	'class'=>'span5',
	'maxlength'=>255)
	)
	)); ?>
</div>
<div>&nbsp;</div>

<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
