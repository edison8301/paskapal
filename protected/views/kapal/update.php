<?php 
	$this->breadcrumbs=array(
	'Kapal'=>array('admin'),
	'Tambah'
);
?>

<?php
	$this->menu=array(
	array('label'=>'List Kapal','url'=>array('index')),
	array('label'=>'Create Kapal','url'=>array('create')),
	array('label'=>'View Kapal','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Kapal','url'=>array('admin')),
	);
?>

	<h1>Sunting Kapal</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>