<?php
$this->breadcrumbs=array(
	'Kapal'=>array('admin'),
	$model->id,
);
?>

<h1>Tampil Kapal</h1>
<div>&nbsp;</div>
<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
				'nama_kapal',
				'tanda_pas',
				'nama_pemilik',
				'alamat_pemilik',
				'npwp_pemilik',
				'ukuran_p',
				'ukuran_l',
				'ukuran_d',
				'tonase_kotor',
				'tonase_bersih',
				'tempat_pembangunan',
				'tahun_pembangunan',
				'bahan_utama',
				'jumlah_geladak',
				'penggerak',
				'mesin_merek',
				'mesin_daya',
		),
)); ?>

<div>&nbsp;</div>
<div class="well">
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'link',
		'url'=>array('update','id'=>$model->id),
		'context'=>'primary',
		'icon'=>'pencil',
		'label'=>'Sunting',
		'htmlOptions'=>array('class'=>'dim')
	)); ?>&nbsp;
	
	 <?php 
		$this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'link',
					'context'=>'success',
					'icon'=>'print',
					'url'=>array('pasKecil/registerPasKecilPdf','id'=>$model->id),
					'label'=>'Cetak Register Pas Kecil',
					'htmlOptions'=>array('target'=>'_blank'),
			));
	 ?>
	 <?php 
		$this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'link',
					'context'=>'success',
					'icon'=>'print',
					'url'=>array('site/mpdf4','id'=>$model->id),
					'label'=>'Cetak Daftar Ukur Dalam Negeri',
					'htmlOptions'=>array('target'=>'_blank'),
			));
	 ?>
</div>
