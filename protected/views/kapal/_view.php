<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kapal')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kapal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanda_pas')); ?>:</b>
	<?php echo CHtml::encode($data->tanda_pas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->nama_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('npwp_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->npwp_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ukuran_p')); ?>:</b>
	<?php echo CHtml::encode($data->ukuran_p); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ukuran_l')); ?>:</b>
	<?php echo CHtml::encode($data->ukuran_l); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ukuran_d')); ?>:</b>
	<?php echo CHtml::encode($data->ukuran_d); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tonase_kotor')); ?>:</b>
	<?php echo CHtml::encode($data->tonase_kotor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tonase_bersih')); ?>:</b>
	<?php echo CHtml::encode($data->tonase_bersih); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_pembangunan')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_pembangunan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun_pembangunan')); ?>:</b>
	<?php echo CHtml::encode($data->tahun_pembangunan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bahan_utama')); ?>:</b>
	<?php echo CHtml::encode($data->bahan_utama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_geladak')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_geladak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penggerak')); ?>:</b>
	<?php echo CHtml::encode($data->penggerak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mesin_merek')); ?>:</b>
	<?php echo CHtml::encode($data->mesin_merek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mesin_daya')); ?>:</b>
	<?php echo CHtml::encode($data->mesin_daya); ?>
	<br />

	*/ ?>

</div>