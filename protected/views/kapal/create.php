<?php
$this->breadcrumbs=array(
	'Kapal'=>array('admin'),
	'Tambah',
);

$this->menu=array(
array('label'=>'List Kapal','url'=>array('index')),
array('label'=>'Manage Kapal','url'=>array('admin')),
);
?>

<h1>Tambah Kapal</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>