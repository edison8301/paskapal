<?php 
$this->breadcrumbs=array(
	'Kapal'=>array('admin'),
	'Kelola',
);
?>

<h1>Kelola Kapal</h1>
<br>
<?php $this->widget('booster.widgets.TbButton', array(
	'buttonType'=>'link',
	'htmlOptions'=>array(),
	'url'=>array('create'),
	'context'=>'primary',
	'icon'=>'plus',
	'label'=>'Tambah',
)); ?>&nbsp;
<?php 
	 	$this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'link',
		'url'=>array('exportExcel'),
		'context'=>'primary',
		'icon'=>'print',
		'label'=>'Export Excel',
	)); ?>&nbsp;
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'kapal-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama_kapal',
			'tanda_pas',
			'nama_pemilik',
			'alamat_pemilik',
			'npwp_pemilik',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
