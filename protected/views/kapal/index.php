<?php
$this->breadcrumbs=array(
	'Kapals',
);

$this->menu=array(
array('label'=>'Create Kapal','url'=>array('create')),
array('label'=>'Manage Kapal','url'=>array('admin')),
);
?>

<h1>Kapals</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
