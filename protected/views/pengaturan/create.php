<?php
$this->breadcrumbs=array(
	'Pengaturans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Pengaturan','url'=>array('index')),
array('label'=>'Manage Pengaturan','url'=>array('admin')),
);
?>

<h1>Create Pengaturan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>