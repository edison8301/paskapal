<?php
$this->breadcrumbs=array(
	'Pengaturans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Pengaturan','url'=>array('index')),
	array('label'=>'Create Pengaturan','url'=>array('create')),
	array('label'=>'View Pengaturan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pengaturan','url'=>array('admin')),
	);
	?>

	<h1>Update Pengaturan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>