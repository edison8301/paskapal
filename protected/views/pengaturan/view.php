<?php
$this->breadcrumbs=array(
	'Pengaturans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pengaturan','url'=>array('index')),
array('label'=>'Create Pengaturan','url'=>array('create')),
array('label'=>'Update Pengaturan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pengaturan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pengaturan','url'=>array('admin')),
);
?>

<h1>View Pengaturan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'kode',
		'nilai',
),
)); ?>
