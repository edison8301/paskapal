<?php
$this->breadcrumbs=array(
	'Pengaturan'

);
?>
<div class="box box-success">
	<div class="box-header with-border">
		<h1 class="box-title">Pengaturan</h1>
	</div>
	<div class="box-body">
		<table id="pengaturan" class="table table-hover table-condensed">
			<?php $i=1; foreach(Pengaturan::model()->findAll() as $data) { ?>		
			<tr>
				<th id="nama-pengaturan" style="text-align: left;width:250px"><?php 
				$nama = str_replace("_", " ", $data->kode);
				print $nama; ?></th>
				<td><?php $this->widget('booster.widgets.TbEditableField',array(
		        		'type' => 'text',
		        		'model' => $data,
		        		'attribute' => 'nilai',
		        		'url' => Yii::app()->controller->createUrl('pengaturan/editableUpdate'),
		    	)); ?></td>
			</tr>
			<?php $i++; } ?>
		</table>
	</div>
</div>