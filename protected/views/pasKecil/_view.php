<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kapal')); ?>:</b>
	<?php echo CHtml::encode($data->id_kapal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tujuan_penggunaan')); ?>:</b>
	<?php echo CHtml::encode($data->tujuan_penggunaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_pemberian')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_pemberian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_pemberian')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_pemberian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_batas_berlaku')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_batas_berlaku); ?>
	<br />


</div>