<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pas-kecil-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Form Pas Kecil</h3>
	</div>
	<div class="box-body">
	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'nomor',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'class'=>'span5',
					'maxlength'=>255
				)
			)
		)); ?>
		</div>
		<div class="col-sm-6">
		<?php echo $form->textFieldGroup($model,'nama_kapal',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'class'=>'span5',
					'maxlength'=>255
				)
			)
		)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'nama_pemilik',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->textAreaGroup($model,'alamat_pemilik',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255,
						'rows'=>3,
					)
				)
			)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'tanda_pas',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'gross_tonnage',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'net_tonnage',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'panjang_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'lebar_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'dalam_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>

			
		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'mesin_penggerak',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'merk_mesin',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'daya_mesin',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'jumlah_geladak',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>	
		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'jumlah_baling_baling',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->select2Group($model,'bahan_utama',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'data'=>BahanUtama::getSelect2List(),
					'htmlOptions'=>array(
						'empty'=>'- Bahan Utama -',
						/* 'ajax' => array(
		                    'type'=>'POST',
		                    'url'=>Yii::app()->controller->createUrl('musrenbang/loadListDesa'), 
		                    'update'=>'#Musrenbang_kode_desa',
		                    'beforeSend' => 'function(){$("#s2id_Musrenbang_kode_kecamatan").append("<i class=\"glyphicon glyphicon-refresh loading\"></i>")}',
							'complete' => 'function(){
								$("#s2id_Musrenbang_kode_kecamatan .loading").hide(); 
							}',*/
		                )),
				)
			); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->select2Group($model,'jenis_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'data'=>JenisKapal::getSelect2List(),
					'htmlOptions'=>array(
						'empty'=>'- Jenis Kapal -',
						/* 'ajax' => array(
		                    'type'=>'POST',
		                    'url'=>Yii::app()->controller->createUrl('musrenbang/loadListDesa'), 
		                    'update'=>'#Musrenbang_kode_desa',
		                    'beforeSend' => 'function(){$("#s2id_Musrenbang_kode_kecamatan").append("<i class=\"glyphicon glyphicon-refresh loading\"></i>")}',
							'complete' => 'function(){
								$("#s2id_Musrenbang_kode_kecamatan .loading").hide(); 
							}',*/
		                )),
				)
			); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'tempat_pembuatan_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>

		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'tahun_pembuatan_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'tempat_register_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'nomor_register_kapal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'tempat_pemberian',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
				'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'tujuan_penggunaan',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->datePickerGroup($model,'tanggal_pemberian',array(
					'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
						'widgetOptions'=>array(
						'options'=>array(
							'format'=>'yyyy-mm-dd',
							'autoclose'=>true),
						'htmlOptions'=>array()
			), 
			'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 
			)); ?>
		</div>
		<div class="col-sm-6">
			<?php echo $form->datePickerGroup($model,'tanggal_batas_berlaku',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
					'widgetOptions'=>array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true),
					'htmlOptions'=>array()
				), 
				'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 
			)); ?>	
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php echo $form->textAreaGroup($model,'catatan',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)); ?>
		</div>
		<div class="col-sm-6">
			
		</div>
	</div>
		
	</div>
	<div class="box-footer with-border">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-3">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'icon'=>'ok',
					'label'=>'Simpan',
				)); ?>
			</div>
		</div>
	</div>
</div>


<?php $this->endWidget(); ?>
