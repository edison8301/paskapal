<style>
	.dotted {
		border-bottom: 2px dotted black;
	}
</style>

<table width="35%">
	<tr>
		<td width="40%"> BUKU KE</td>
		<td width="10%">:</td>
		<td class="dotted" style="text-align:center">1</td>
	</tr>
	<tr>
		<td width="40%"> NO. HALAMAN</td>
		<td width="10%">:</td>
		<td class="dotted" style="text-align:center">747</td>
	</tr>
	<tr>
		<td width="40%"> NO. URUT</td>
		<td width="10%">:</td>
		<td class="dotted" style="text-align:center">747</td>
	</tr>
</table>
<br/>

<h1 style="text-align: center">REGISTER PAS KECIL</h1>

<table width="100%" >
	<tr>
		<td width="25%"> TANGGAL PENERBITAN</td>
		<td width="3%">:</td>
		<td class="dotted"><?php print $paskecil->tanggal_pemberian ?></td>
	</tr>
	<tr>
		<td width="25%"> TEMPAT PENERBITAN</td>
		<td width="3%">:</td>
		<td class="dotted"><?php print $paskecil->tempat_pemberian ?></td>
	</tr>
	<tr>
		<td width="25%"> NAMA KAPAL</td>
		<td width="3%">:</td>
		<td class="dotted"><?php print $paskecil->nama_kapal ?></td>
	</tr>		
	<tr>
		<td width="25%"> TANDA PAS KECIL</td>
		<td width="3%">:</td>
		<td class="dotted"><?php print $paskecil->tanda_pas ?></td>
	</tr>	
</table>

<div style="border-bottom:1px solid black; margin-top:7px; margin-bottom:7px;"></div>

<table width="100%">
<tr>
	<td width="20%">NAMA PEMILIK </td>
	<td width="3%">:</td>
	<td class="dotted" style="text-transform:uppercase;font-weight:bold;"><?php print $paskecil->nama_pemilik ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td style="font-weight:bold;">BERKEDUDUKAN/BERALAMAT DI</td>

</tr>
<tr>
	<td></td>
	<td></td>
	<td class="dotted" style="font-style:italic;text-transform:uppercase;"><?php print $paskecil->alamat_pemilik ?> </td>
	
</tr>
</table>
<br/>
<br/>
<br/>
<div style="border-bottom:1px solid black; margin-top:7px; margin-bottom:7px;"></div>

<h1 style="text-align: center;">DATA KAPAL</h1>

<table width="100%">
	<tr>
		<td width="33%">UKURAN P X L X D</td>
		<td width="3%">:</td>
		<td align="center" class="dotted" width="7%"><?php print $paskecil->panjang_kapal ?></td>
		<td width="2%">X</td>
		<td align="center" class="dotted" width="7%"><?php print $paskecil->lebar_kapal ?></td>
		<td width="2%">X</td>
		<td align="center" class="dotted" width="7%"><?php print $paskecil->dalam_kapal ?> </td>
		<td>Meter</td>
		<td width="3%"></td>
		<td></td>
	</tr>
	<tr>
		<td>TONASE KOTOR (GT)</td>
		<td>:</td>
		<td class="dotted" colspan="5"><b><?php print $paskecil->gross_tonnage ?></b></td>
		<td>TONASE BERSIH (NT)</td>
		<td>:</td>
		<td class="dotted"><b><?php print $paskecil->net_tonnage ?></b></td>
	</tr>
	<tr>
		<td>TEMPAT DAN TAHUN PEMBANGUNAN</td>
		<td>:</td>
		<td class="dotted" colspan="4"><?php print $paskecil->tempat_pembuatan_kapal ?><b></b></td>
		<td class="dotted" colspan="4">&nbsp; TAHUN &nbsp; <?php print $paskecil->tahun_pembuatan_kapal ?></td>
	</tr>
	<tr>
		<td>BAHAN UTAMA</td>
		<td>:</td>
		<td class="dotted" colspan="5"><?php print $paskecil->bahan_utama ?></td>
		<td>JUMLAH GELADAK</td>
		<td>:</td>
		<td class="dotted"><?php print $paskecil->jumlah_geladak ?></td>
	</tr>
	<tr>
		<td>PENGGERAK UTAMA</td>
		<td>:</td>
		<td class="dotted" colspan="5"><?php print $paskecil->mesin_penggerak ?></td>
		<td>JUMLAH BALING-BALING</td>
		<td>:</td>
		<td class="dotted"><?php print $paskecil->jumlah_baling_baling ?></td>
	</tr>
	<tr>
		<td>MESIN INDUK</td>
		<td>:</td>
		<td>MEREK</td>
		<td class="dotted" colspan="4"><?php print $paskecil->merk_mesin ?></td>
		<td>DENGAN DAYA</td>
		<td>:</td>
		<td class="dotted"><?php print $paskecil->daya_mesin ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td></td>
		<td colspan="5">&nbsp;</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td></td>
		<td colspan="5">&nbsp;</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td></td>
		<td colspan="5">&nbsp;</td>
		<td>&nbsp;</td>
		<td></td>
		<td>&nbsp;</td>
	</tr>
</table>
<div style="border-bottom:1px solid black; margin-top:7px; margin-bottom:7px;"></div>
<h3>CATATAN :</h3>
<br/>
<table width="100%">
	<tr>
		<td class="dotted">&nbsp;</td>
	</tr>
	<tr>
		<td class="dotted"><?php print $paskecil->catatan ?></td>
	</tr>
	<tr>
		<td class="dotted">&nbsp;</td>
	</tr>
	<tr>
	<tr>
		<td class="dotted">&nbsp;</td>
	</tr>
	<tr>
	<tr>
		<td class="dotted">&nbsp;</td>
	</tr>
	<tr>
</table>
