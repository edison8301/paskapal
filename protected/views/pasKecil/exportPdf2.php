<h1 style="text-align: center;">PAS - KECIL</h1>

<table align="center">
 	<tr>
		<td style="font-weight: bold;">NO</td>
		<td style="font-weight: bold;">: </td>
		<td style="font-weight: bold; border-bottom: 2px dotted black"><?php print $paskecil->nomor; ?></td>
	</tr>
</table>


	<table width="100%" style="text-align: center; font-weight: bold; font-size: 17px">
		<tr>
			<td style="width: 70%">&nbsp;</td>
			<td>Republik</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Indonesia</td>
		</tr>
	</table>




<div>&nbsp;</div>
<p style="text-align: justify;">Yang bertanda tangan dibawah ini <span style="font-weight: bold; border-bottom: 2px dotted black">Kepala Dinas Perhubungan Kab. Belitung Timur</span> menyatakan bahwa</p><br/>

<table width="100%" border="1" align="justify"  cellspacing="0">


	<tr>
		<th style="font-size: 14px; font-style: italic" valign="top" height="50px">NAMA KAPAL</th>
		<th style="font-size: 14px; font-style: italic" valign="top" height="50px">TANDA PASS</th>
		<th style="font-size: 14px; font-style: italic" valign="top" height="50px">GROSS TONNAGE (GT)</th>
		<th style="font-size: 14px; font-style: italic" valign="top" height="50px">P x L x D</th>
	</tr>
	
	<tr>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->nama_kapal ?></td>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->tanda_pas ?></td>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->gross_tonnage ?></td>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->panjang_kapal ?> X <?php print $paskecil->lebar_kapal ?> X <?php print $paskecil->dalam_kapal ?></td>
	</tr>

</table><br/>

<table width="100%" border="1" align="center" cellspacing="0" >
	<tr>
		<th style="font-size: 14px; padding-left: 10px; padding-right: 10px" valign="top" height="50px">PENGGERAK</th>
		<th style="font-size: 14px; padding-left: 10px; padding-right: 10px; width: 20%" valign="top" height="50px">MERK. TK/KW</th>
		<th style="font-size: 14px; padding-left: 10px; padding-right: 10px" valign="top" height="50px">BAHAN UTAMA</th>
		<th style="font-size: 14px; padding-left: 10px; padding-right: 10px" valign="top" height="50px">JUMLAH GELADAK</th>
		<th style="font-size: 14px; padding-left: 10px; padding-right: 10px" valign="top" height="50px">TAHUN PEMBANGUNAN</th>
	</tr>
	<tr>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->mesin_penggerak ?></td>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->merk_mesin ?></td>
		<td align="center" valign="middle" height="60px"><?php print PasKecil::getBahanUtama($paskecil->bahan_utama) ?></td>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->jumlah_geladak ?></td>
		<td align="center" valign="middle" height="60px"><?php print $paskecil->tahun_pembuatan_kapal ?></td>
	</tr>
</table>
&nbsp;
<div style="text-align: justify;">

<table  width="100%" style="text-align: justify">
	<tr>
		<td width="20%">Dipergunakan Sebagai </td>
		<td width="1%">:</td>
		<td width="60%"><span style="font-weight: bold; border-bottom: 2px dotted black"><?php print $paskecil->tujuan_penggunaan; ?></span></td>
	</tr>
	<tr>
		<td>Nama Pemilik </td>
		<td>:</td>
		<td><span style="font-weight: bold; border-bottom: 2px dotted black"><?php print $paskecil->nama_pemilik; ?></span></td>
	</tr>
	<tr>
		<td>Alamat Pemilik</td>
		<td>:</td>
		<td><span style="font-weight: bold; border-bottom: 2px dotted black"><?php print $paskecil->alamat_pemilik; ?></span></td>
	</tr>
	<tr>
		<td>Lokasi Register Pas Kapal</td>
		<td>:</td>
		<td><span style="font-weight: bold; border-bottom: 2px dotted black"><?php print $paskecil->tempat_register_kapal ?></span></td>
	</tr>
	<tr>
		<td>Dengan Nomor</td>
		<td>:</td>
		<td><span style="font-weight: bold; border-bottom: 2px dotted black"><?php print $paskecil->nomor ?></span></td>
	</tr>
	<tr>
		<td>Berlaku Sampai Tanggal</td> 
		<td>:</td>
		<td><span style="font-weight: bold; border-bottom: 2px dotted black"><?php print Helper::getTanggal($paskecil->tanggal_batas_berlaku); ?></span></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			 Kepada seluruh pejabat Republik Indonesia dan mereka yang bersangkutan diharap supaya
			 memperlakukan nahkoda kapal dan muatannya sesuai ketentuan Undang-Undang Republik Indonesia
			 dan Perjanjian-Perjanjian dengan Negara-Negara Lain.
		</td>
	</tr>

</table>


<table  align="right" width="50%">
	<tr>
		<td width="100px" >Diberikan di </td>
		<td width="5px">:</td>
		<td style="text-align: center; border-bottom: 2px dotted black"><?php print $paskecil->tempat_pemberian ?></td>
	</tr>
	<tr>
		<td>Pada Tanggal</td>
		<td>:</td>
		<td style="text-align: center; border-bottom: 2px dotted black"><?php print Helper::getTanggal($paskecil->tanggal_pemberian); ?></td>
	</tr>

</table>
<div>&nbsp;</div>
<table  align="right" border="0" width="50%">
	<tr>
		<td colspan="2" align="center" valign="middle">An.BUPATI BELITUNG TIMUR</td>
		<td>&nbsp;</td>
	
	</tr>
	<tr>
		<td colspan="2" align="center" valign="middle">KEPALA DINAS PERHUBUNGAN</td>
		<td>&nbsp;</td>
		
	</tr>
	<tr>
		<td colspan="2" align="center" valign="middle">KABUPATEN BELITUNG TIMUR</td>
		<td>&nbsp;</td>
	
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center" valign="middle"><?php print Pengaturan::getPengaturan('penandatangan_nama'); ?></td>
	</tr>
	<tr>
		<td colspan="2" align="center" valign="middle">NIP <?php print Pengaturan::getPengaturan('penandatangan_nip'); ?></td>
	</tr>
</table>
