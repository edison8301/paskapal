<?php
$this->breadcrumbs=array(
	'Pas-Kecil'=>array('admin'),
	'Export',
);
?>

<h1>Export Pas-Kecil</h1>

<p>Silahkan tentukan tanggal awal dan tanggal akhir</p>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
				'id'=>'form',
				'type'=>'horizontal',
				'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		)); ?>
<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

<div class="well">
	<?php echo $form->select2Group($model,'berdasarkan_tanggal',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
		'widgetOptions'=>array(
			'data'=>array(
				    1=>'Tanggal Pemberian',
				    2=>'Tanggal Batas Berlaku',
				  ),
			'htmlOptions'=>array(
				'empty'=>'- Pilih Tanggal -',
                )),
		)
	); ?>
	<?php echo $form->datePickerGroup($model,'tanggal_awal',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
				'options'=>array(
					'format'=>'yyyy-mm-dd',
					'autoclose'=>true),
				'htmlOptions'=>array()
	), 
	'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 
	)); ?>
	<?php echo $form->datePickerGroup($model,'tanggal_akhir',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
				'options'=>array(
					'format'=>'yyyy-mm-dd',
					'autoclose'=>true),
				'htmlOptions'=>array(),
	), 
	'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 
	)); ?>
</div>

<div class="action-forms well" style="">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'context'=>'primary',
						'icon'=>'download-alt',
						'label'=>'Export',
				)); ?>&nbsp;
				<?php $this->widget('booster.widgets.TbButton',array(
					'buttonType'=>'link',
					'url'=>Yii::app()->request->urlReferrer,
					'label'=>'Kembali',
					'context'=>'success',
					'icon'=>'arrow-left'
				)); ?>
			</div>
		</div>
</div>

<?php $this->endWidget(); ?>



