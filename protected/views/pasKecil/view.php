<?php
$this->breadcrumbs=array(
	'Pas-Kecil'=>array('admin'),
	'View'
);
	
?>

<h1>Lihat Pas-Kecil</h1>

<div>&nbsp;</div>

	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'link',
		'url'=>array('update','id'=>$paskecil->id),
		'context'=>'primary',
		'icon'=>'pencil',
		'label'=>'Sunting',
		'htmlOptions'=>array('class'=>'dim')
	)); ?>&nbsp;
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'context'=>'success',
			'icon'=>'print',
			'url'=>array('exportPdf2','id'=>$paskecil->id),
			'label'=>'Cetak Pas Kecil',
			'htmlOptions'=>array('target'=>'_blank'),
	)); ?>

	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'context'=>'success',
			'icon'=>'print',
			'url'=>array('registerPasKecilPdf','id'=>$paskecil->id),
			'label'=>'Cetak Register Pas Kecil',
			'htmlOptions'=>array('target'=>'_blank'),
	)); ?>

	<?php /* $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'context'=>'success',
			'icon'=>'print',
			'url'=>array('export'),
			'label'=>'Export Excel',
	)); <- dipindahkan ke halaman Pas-Kecil/admin */?>

<div>&nbsp;</div>
<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$paskecil,
'type'=>'striped bordered condensed',
'attributes'=>array(
		'nomor',
		'nama_kapal',
		'nama_pemilik',
		'alamat_pemilik',		
		'tujuan_penggunaan',
		'tempat_pemberian',
		array(
			'name' => 'tanggal_pemberian',
			'value' => Helper::getTanggal($paskecil->tanggal_pemberian)
			),
		array(
			'name' => 'tanggal_batas_berlaku',
			'value' => Helper::getTanggal($paskecil->tanggal_batas_berlaku)
			),
		'tanda_pas',
		'gross_tonnage',
		'net_tonnage',
		'panjang_kapal',
		'lebar_kapal',
		'dalam_kapal',
		'mesin_penggerak',
		'merk_mesin',
		'daya_mesin',
		array(
			'name' => 'bahan_utama',
			'value' => PasKecil::getBahanUtama($paskecil->bahan_utama)
			),
		'jumlah_geladak',
		'jumlah_baling_baling',
		array(
			'name' => 'jenis_kapal',
			'value' => PasKecil::getJenisKapal($paskecil->jenis_kapal)
			),
		'tempat_pembuatan_kapal',
		'tahun_pembuatan_kapal',
		'tempat_register_kapal',
		'nomor_register_kapal',
		'catatan',
),
)); ?>

<div>&nbsp;</div>
<?php 
// $this->widget('booster.widgets.TbDetailView',array(
// 		'data'=>$kapal,
// 		'type'=>'striped bordered condensed',
// 		'attributes'=>array(
// 				'nama_kapal',
// 				'tanda_pas',
// 				'nama_pemilik',
// 				'alamat_pemilik',
// 				'npwp_pemilik',
// 				'ukuran_p',
// 				'ukuran_l',
// 				'ukuran_d',
// 				'tonase_kotor',
// 				'tonase_bersih',
// 				'tempat_pembangunan',
// 				'tahun_pembangunan',
// 				'bahan_utama',
// 				'jumlah_geladak',
// 				'penggerak',
// 				'mesin_merek',
// 				'mesin_daya',
// 		),
// )); ?>


<div>&nbsp;</div>
