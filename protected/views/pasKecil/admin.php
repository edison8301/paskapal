<?php
$this->breadcrumbs=array(
	'Pas-Kecil'=>array('admin'),
	'Kelola',
	);
?>

<div class="box box-success">
	<div class="box-header with-border">
		<?php 
			$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array(),
			'url'=>array('create'),
			'context'=>'success',
			'icon'=>'plus',
			'label'=>'Tambah',
		)); ?>&nbsp;
		<?php 
		 	$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'url'=>array('export'),
			'context'=>'success',
			'icon'=>'print',
			'label'=>'Export Excel',
		)); ?>&nbsp;
	</div>
	<div class="box-body">
		<?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'pas-kecil-grid',
			'type'=>'striped bordered condensed',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
				array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
				),	
				'nomor',
				'nama_pemilik',
				'tujuan_penggunaan',
				array(
				'name'=>'tanggal_pemberian',
				'type'=>'raw',
				'value'=>'Helper::getTanggal($data->tanggal_pemberian)',
				),
				array(
				'name'=>'tanggal_batas_berlaku',
				'type'=>'raw',
				'value'=>'Helper::getTanggal($data->tanggal_batas_berlaku)',
				),
				array(
					'class'=>'booster.widgets.TbButtonColumn',
				),
			),
		)); ?>
	</div>
</div>