<?php
$this->breadcrumbs=array(
	'Pas-Kecil'=>array('admin'),
	'Tambah'
	);

$this->menu=array(
array('label'=>'List PasKecil','url'=>array('index')),
array('label'=>'Manage PasKecil','url'=>array('admin')),
);

$this->pageTitle = 'Tambah Pas Kecil';
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>