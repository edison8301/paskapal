<?php
/* @var $this SiteController */

$this->breadcrumbs=array(
	'Dashboard',
);


$this->pageTitle=Yii::app()->name;
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/fusioncharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js"); ?>
 <script>
	FusionCharts.ready(function(){
		var revenueChart = new FusionCharts({
			"type": "Column3d",
			"renderAt": "kapal",
			"width": "100%",
			"height": "300",
			"dataFormat": "json",
			"dataSource": {
			  "chart": {
				  "caption" : "Jumlah Pendaftaran Pas-Kecil",
				  "xAxisName": "Bulan",
				  "yAxisName": "Jumlah",
				  "theme": "fint"
			   },
			  "data":        
					[ <?php 
						print PasKecil::getGraphListPendaftaran();
					  ?> 
					]
			}
	});
	revenueChart.render();
})

	FusionCharts.ready(function(){
		  var revenueChart = new FusionCharts({
			"type": "column3d",
			"renderAt": "kapal2",
			"width": "100%",
			"height": "300",
			"dataFormat": "json",
			"dataSource": {
			  "chart": {
				  "caption" : "Jumlah Kapal per Jenis Kapal",
				  "xAxisName": "Jenis Kapal",
				  "yAxisName": "Jumlah Kapal",
				  "theme": "fint"
			   },
			  "data":        
				  [ <?php 
						print PasKecil::getGraphListJenisKapal();
					?>
				  ]                           
			}
		});
		revenueChart.render();
	})

	FusionCharts.ready(function(){
		  var revenueChart = new FusionCharts({
			"type": "column3d",
			"renderAt": "kapal3",
			"width": "100%",
			"height": "300",
			"dataFormat": "json",
			"dataSource": {
			  "chart": {
				  "caption" : "Jumlah Kapal per Bahan Utama",
				  "xAxisName": "Bahan Utama",
				  "yAxisName": "Jumlah Kapal",
				  "theme": "fint"
			   },
			  "data":        
				  [ <?php 
						print PasKecil::getGraphListBahanUtama();
					?>
				  ]                           
			}
		});
		revenueChart.render();
	})

	FusionCharts.ready(function(){
		  var revenueChart = new FusionCharts({
			"type": "column3d",
			"renderAt": "kapal4",
			"width": "100%",
			"height": "300",
			"dataFormat": "json",
			"dataSource": {
			  "chart": {
				  "caption" : "Jumlah Kapal per Mesin Penggerak",
				  "xAxisName": "Mesin Penggerak",
				  "yAxisName": "Jumlah Kapal",
				  "theme": "fint"
			   },
			  "data":        
				  [ <?php 
						print PasKecil::getGraphListMesinPenggerak();
					?>
				  ]                           
			}
		});
		revenueChart.render();
	})
</script>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Selamat Datang di Aplikasi PAS KAPAL</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-sm-3">
                <div class="small-box bg-green">
                    <div class="inner">
                        <p>Jumlah Pas Kapal Hari Ini</p>
                        <h3 style="font-size:24px"><span id=""><?php print PasKecil::countPasKapalHariIni(); ?></span> Pas Kapal</h3>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-stats"></i>
                    </div>
                    <a href="<?php echo Yii::app()->createUrl('pasKecil/admin');?>" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div><!-- .small-box -->
            </div>
            <div class="col-sm-3">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <p>Jumlah Pas Kapal Minggu Ini</p>
                        <h3 style="font-size:24px"><span id=""><?php print PasKecil::countPasKapalMingguIni(); ?></span> Pas Kapal</h3>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-stats"></i>
                    </div>
                    <a href="<?php echo Yii::app()->createUrl('pasKecil/admin');?>" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div><!-- .small-box -->
            </div>
            <div class="col-sm-3">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <p>Jumlah Pas Kapal Bulan Ini</p>
                        <h3 style="font-size:24px"><span id=""><?php print PasKecil::countPasKapalBulanIni(); ?></span> Pas Kapal</h3>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-stats"></i>
                    </div>
                    <a href="<?php echo Yii::app()->createUrl('pasKecil/admin');?>" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div><!-- .small-box -->
            </div>
            <div class="col-sm-3">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <p>Jumlah Pas Kapal Tahun Ini</p>
                        <h3 style="font-size:24px"><span id=""><?php print PasKecil::countPasKapalTahunIni(); ?></span> Pas Kapal</h3>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-stats"></i>
                    </div>
                    <a href="<?php echo Yii::app()->createUrl('pasKecil/admin');?>" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div><!-- .small-box -->
            </div>
		</div>
	</div>
</div>
<div class="box box-success">
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Grafik Pas-Kecil
					</div>
					<div class="panel-body">
						<div id="kapal"> FusionChart XT will load here! </div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Jenis Kapal
					</div>
					<div class="panel-body">
						<div id="kapal2"> FusionChart XT will load here! </div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Grafik Pas-Kecil
					</div>
					<div class="panel-body">
						<div id="kapal3"> FusionChart XT will load here! </div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Grafik Pas-Kecil
					</div>
					<div class="panel-body">
						<div id="kapal4"> FusionChart XT will load here! </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	



