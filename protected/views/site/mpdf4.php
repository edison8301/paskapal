<style>
	td {
		padding: 2px;
	}
</style>
<div style="border: 2px solid black;padding:10px;">
<h3 style="text-align: center">PENGUKURAN KAPAL</h3>
<h3 style="text-align: center">PERATURAN MENTERI PERHUBUNGAN NOMOR 6 TAHUN 2005</h3>
<table width="100%" border="">
	<tr>
		<td width="50%">Kedudukan pengukuran kapal: MANGGAR <br>
			Tempat dan tanggal pengukuran <br>
			MANGGAR...................3/1/2011
		</td>
		
		<td width="50%" style="padding-left: 170px;">
			Pengukuran Pertama <br>
			Pengukuran ulangan disebabkan <br>
			.......................... <br>
			No. Surat Ukur Terdahulu ....
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<br>	
			<h1>DAFTAR UKUR DALAM NEGERI</h1>
		</td>
	</tr>
	<tr>
		<td valign="bottom" style="padding-bottom: 10px;">
			No. DU.193/DISHUB/III2011
		</td>
		<td style="padding-left: 160px;">
			<table border="" width="100%">
				<tr>
					<td>
					<center><b>Nama Kapal</b></center>
					</td>
				</tr>
				<tr>
					<td>
					KM. Putri Jaya ................................... <br>
					<b>Eks</b>....................................................
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="1" valign="center" cellspacing="0">	
	<tr>
		<td align="center">
			<i><b>Pelabuhan <br>
			Pendaftaran</b></i>
		</td>
		<td align="center">
			<i><b>Jenis Kapal</i></b>
		</td>
		<td align="center">
			<i><b>Nama <br>
			Panggilan</i></b>
		</td>
		<td align="center">
			<i><b>Digerakkan oleh <br>
			mesin atau layar</b></i>
		</td>
		<td align="center">
			<i><b>Bahan</i></b>
		</td>
	</tr>
	<tr>
		<td align="center" height=60px>
			-
		</td>
		<td align="center" height=60px>
			KAPAL MOTOR
		</td>
		<td align="center" height=60px>
			-
		</td>
		<td align="center" height=60px>
			MOTOR
		</td>
		<td align="center" height=60px>
			KAYU
		</td>
	</tr>
	<tr>
		<td align="center">
			<i><b>Tempat dan Tanggal <br>
			Peletakan Lunas</b></i>
		</td>
		<td colspan="3" align="center">
			<i><b>Nama dan Alamat Pembangunan</b></i>
		</td>
		<td align="center">
			<i><b>Nomor <br>
			Galangan</b></i>
		</td>
	</tr>
	<tr>
		<td align="center" height=60px>
			-
		</td>
		<td align="center" height=60px colspan="3">
			JL. JENDERAL SUDIRMAN RT 024/010 DESA BARU MANGGAR - BELTIM
		</td>
		<td align="center" height=60px>
			-
		</td>
	</tr>
	<tr>
		<td align="center">
			<i><b>Keterangan Alat<br>
			Penggerak</b></i>
		<td align="center">
			<i><b>Jumlah Baling - <br>
			Baling</i></b>
		</td>
		<td align="center">
			<i><b>Jumlah <br>
			Cerobong Asap</i></b>
		</td>
		<td align="center">
			<i><b>Jumlah Geladak</b></i>
		</td>
		<td align="center">
			<i><b>Jumlah Tiang</i></b>
		</td>
	</tr>
	<tr>
		<td align="center" height=60px>
			ISUZU, 151820
		</td>
		<td align="center" height=60px>
			1
		</td>
		<td align="center" height=60px>
			1
		</td>
		<td align="center" height=60px>
			1
		</td>
		<td align="center" height=60px>
			1
		</td>
	</tr>
	<tr>
		<td height="60px" width="25%" style="border-right:none;" valign="top"><i><b>Nama dan Alamat Pemilik:</b></i></td>
		<td height="60px" colspan="4" style="border-left:none;" align="center">==LAODE RUSLI == DSN. BARU UTARA RT 003/002 DESA BARU MANGGAR BELTIM</td>
	</tr>
	<tr>
		<td colspan="5" align="center">
			<i><b>UKURAN-UKURAN POKOK</b></i>
		</td>
	</tr>
	<tr>
		<td valign="top" height="60px" style="border-right: none;">
			Panjang	:
		</td>
		<td valign="top" colspan="3" height="60px" style="border-left: none;">
			adalah jarak mendarat antara titik temu sisi luar kulit lambung dengan tinggi haluan dan tinggi buritan pada ketinggian geladak atas atau pada bagian sebelah atas dari rimbat tetap.
		</td>
		<td align="center" height="60px">
			9,9 M
		</td>
	</tr>
	<tr>
		<td valign="top" height="60px" style="border-right: none;">
			Lebar	:
		</td>
		<td valign="top" colspan="3" height="60px" style="border-left: none;">
			adalah jarak mendatar antara kedua sisi kulit lambung pada bagian kapal yang terlebar, tidak termasuk pisang-pisang.
		</td>
		<td align="center" height="60px">
			2,4 M
		</td>
	</tr>
	<tr>
		<td valign="top" height="60px" style="border-right: none;">
			Dalam	:
		</td>
		<td valign="top" colspan="3" height="60px" style="border-left: none;">
			adalah jarak tegak lurus di tengah - tengah lebar pada bagian kapal yang terlebar dari sebelah bawah alus lunas sampai bagian bawah geladak atau sampai garis melintang kapal yang ditarik melalui kedua sisi atas rimbat tetap
		</td>
		<td align="center" height="60px">
			9,9 M
		</td>
	</tr>
	<tr>
		<td style="border-right: none;border-bottom: none;">
			
		</td>
		<td colspan="4" style="border-left: none;border-bottom: none; padding-left: 210px;">
			<p style="font-size: 12px;"><b>TONNASE KAPAL ADALAH <br>
										ISI KOTOR (GT)  : ......4......... <br>
										ISI BERSIH (NT) : ......1,2....... </b></p>
		</td>
	</tr>
	<tr>
		<td colspan="5" style="padding-left: 400px;padding-top: 10px; border-top: none;border-bottom: none">
			Dikeluarkan di <b> MANGGAR </b> 3/1/2011 .................

		</td>
	</tr>
	<tr>
		<td colspan="5" style="padding-left: 380px; border-top: none;" align="center" >
			<p>Pengukur Kapal,</p><br><br><br><br>
			<p  style="margin-top: 50px; font-weight: bold">ARY SUHERY</p><br>
			<p>NIP. 19831023 200904 1 002 </p>
		</td>
	</tr>
</table>
</div>