<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'type'=>'horizontal',
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<div class="well">
	<?php echo $form->textFieldGroup($model,'username',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255
				))
	)); ?>

	<?php echo $form->passwordFieldGroup($model,'password',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255
				))
	)); ?>

	<?php echo $form->select2Group($model,'id_role',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Role::model()->findAll(),'id','role'),
				'htmlOptions'=>array('empty'=>'- Pilih Status -')
		)
	)); ?>
</div>

<div class="form-actions well">
	<div class="row">
		<div class="col-sm-3"></div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'context'=>'primary',
						'label'=>'Simpan',
					)); ?>
			</div>
	</div>
</div>

<?php $this->endWidget(); ?>
