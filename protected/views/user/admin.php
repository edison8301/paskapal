<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'User'=>array('admin'),
	'Kelola',
);
?>
<div class="box box-success">
	<div class="box-header with-border">
		<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'success',
			'icon'=>'plus',
			'label'=>'Tambah User',
			'url'=>array('create')
		)); ?>&nbsp;
	</div>
	<div class="box-body">
		<?php $this->widget('booster.widgets.TbGridView', array(
			'id'=>'user-grid',
			'dataProvider'=>$model->search(),
			'type'=>'striped bordered condensed',
			'filter'=>$model,
			'columns'=>array(
				array(
					'header'=>'No',
					'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
					'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
					'htmlOptions'=>array('style'=>'text-align:center'),
					),	
				'username',
				array(
					'name'=>'id_role',
					'header'=>'Role',
					'type'=>'raw',
					'value'=>'$data->getRelation("role","role")',
					),
				array(
					'class'=>'booster.widgets.TbButtonColumn',
					'template'=>'{update},{delete}',
				),
			),
		)); ?>

	</div>
</div>