<?php
$this->breadcrumbs=array(
	'Bahan Utama'=>array('index'),
	'Kelola',
);
?>

<h1>Kelola Bahan Utama</h1>

<div>&nbsp;</div>

<?php 
	$this->widget('booster.widgets.TbButton', array(
	'buttonType'=>'link',
	'htmlOptions'=>array(),
	'url'=>array('create'),
	'context'=>'primary',
	'icon'=>'plus',
	'label'=>'Tambah',
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'bahan-utama-grid',
	'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
			'htmlOptions'=>array('style'=>'text-align:center'),
			),	
			'nama',
	array(
	'class'=>'booster.widgets.TbButtonColumn',
	'template'=>'{update},{delete}'
	),
	),
)); ?>
