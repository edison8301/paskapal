<?php
$this->breadcrumbs=array(
	'Bahan Utama'=>array('index'),
	'Tambah',
);

$this->menu=array(
array('label'=>'List BahanUtama','url'=>array('index')),
array('label'=>'Manage BahanUtama','url'=>array('admin')),
);
?>

<h1>Tambah Data Bahan Utama</h1>
<div>&nbsp;</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>