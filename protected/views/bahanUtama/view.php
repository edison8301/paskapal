<?php
$this->breadcrumbs=array(
	'Bahan Utamas'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List BahanUtama','url'=>array('index')),
array('label'=>'Create BahanUtama','url'=>array('create')),
array('label'=>'Update BahanUtama','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete BahanUtama','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage BahanUtama','url'=>array('admin')),
);
?>

<h1>View BahanUtama #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
