<?php
/* @var $this RetribusiController */
/* @var $model Retribusi */

$this->breadcrumbs=array(
	'Retribusi'=>array('admin'),
	'Tambah',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>