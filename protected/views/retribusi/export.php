<?php
$this->breadcrumbs=array(
	'Retribusi'=>array('admin'),
	'Export',
);
?>

<h1>Export Retribusi</h1>

<p>Silahkan tentukan tanggal awal dan tanggal akhir</p>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
				'id'=>'form',
				'type'=>'horizontal',
				'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		)); ?>
<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

<div class="well">
	<?php echo $form->select2Group($model,'berdasarkan_tanggal',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
		'widgetOptions'=>array(
			'data'=>array(
				    1=>'Tanggal Retribusi',
				    2=>'Tanggal Jatuh Tempo',
				    3=>'Tanggal Masa Berlaku Uji',
				    4=>'Tanggal Batas Berlaku Uji',
				  ),
			'htmlOptions'=>array(
				'empty'=>'- Pilih Tanggal -',
                )),
		)
	); ?>
	<?php echo $form->datePickerGroup($model,'tanggal_awal',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
				'options'=>array(
					'format'=>'yyyy-mm-dd',
					'autoclose'=>true),
				'htmlOptions'=>array()
	), 
	'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 
	)); ?>
	<?php echo $form->datePickerGroup($model,'tanggal_akhir',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
				'options'=>array(
					'format'=>'yyyy-mm-dd',
					'autoclose'=>true),
				'htmlOptions'=>array(),
	), 
	'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 
	)); ?>
</div>

<div class="action-forms well" style="">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'context'=>'primary',
						'icon'=>'download-alt',
						'label'=>'Export',
				)); ?>&nbsp;
				<?php $this->widget('booster.widgets.TbButton',array(
					'buttonType'=>'link',
					'url'=>Yii::app()->request->urlReferrer,
					'label'=>'Kembali',
					'context'=>'success',
					'icon'=>'arrow-left'
				)); ?>
			</div>
		</div>
</div>

<?php $this->endWidget(); ?>

<?php /* print CHtml::beginForm(array('retribusi/exportExcel')); ?>

<div class="well">
	<div class="form-group row">
		<div class="col-sm-3" style="text-align:right">
			<?php print CHtml::label('Export Berdasarkan',''); ?>
		</div>
		<div class="col-sm-9">
			<?php $this->widget('booster.widgets.TbSelect2',
				array(
				  'name'=>'tanggal_jenis',
				  'data'=>array(
				    1=>'Tanggal Retribusi',
				    2=>'Tanggal Jatuh Tempo',
				    3=>'Tanggal Masa Berlaku Uji',
				    4=>'Tanggal Batas Berlaku Uji',
				  ),
				  'htmlOptions'=>array(
				  	'style'=>'width: 200px;',
				  	)
			)); ?>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-3" style="text-align:right">
			<?php print CHtml::label('Tanggal Awal','',array('class'=>'control-label')); ?>
		</div>
		<div class="col-sm-4">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'name'=>'tanggal_awal',
					// additional javascript options for the date picker plugin
					'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd',
						'changeYear'=>true,
						'changeMonth'=>true,
					),
					'htmlOptions'=>array(
						'class'=>'form-control',
						'placeholder'=>'Tanggal Awal',
					),
					'value'=>date('Y-m-d'),
			)); ?>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-3" style="text-align:right">
			<?php print CHtml::label('Tanggal Akhir',''); ?>
		</div>
		<div class="col-sm-4">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'name'=>'tanggal_akhir',
					// additional javascript options for the date picker plugin
					'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd',
						'changeYear'=>true,
						'changeMonth'=>true,
						
					),
					'htmlOptions'=>array(
						'class'=>'form-control',
						'placeholder'=>'Tanggal Akhir',

					),
					'value'=>date('Y-m-d'),
					
			)); ?>
		</div>
	</div>
	
	<div>&nbsp;</div>

</div>
	<div class="col-sm-12 form-actions well">
		<?php $this->widget('booster.widgets.TbButton',array(
				'buttonType'=>'submit',
				'label'=>'Export',
				'context'=>'success',
				'icon'=>'download-alt'
		)); ?>&nbsp;
		<?php $this->widget('booster.widgets.TbButton',array(
				'buttonType'=>'link',
				'url'=>Yii::app()->request->urlReferrer,
				'label'=>'Kembali',
				'context'=>'success',
				'icon'=>'arrow-left'
		)); ?>
	</div>
</div>

<?php print CHtml::endForm(); */?>