
<div class="box box-success">
	<div class="box-header with-border">
		<?php
			$this->breadcrumbs=array(
			'Retribusi'=>array('admin'),
			'Kelola',
			);

			 $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array(),
			'url'=>array('create'),
			'context'=>'success',
			'icon'=>'plus',
			'label'=>'Tambah',
		)); ?>&nbsp;
		<?php 
		 	$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'url'=>array('export'),
			'context'=>'success',
			'icon'=>'print',
			'label'=>'Export Excel',
		)); ?>&nbsp;
	</div>
	<div class="box-body">
		<?php $this->widget('booster.widgets.TbGridView', array(
			'id'=>'retribusi-grid',
			'type'=>'striped bordered',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
				array(
					'header'=>'No',
					'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
					'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
					'htmlOptions'=>array('style'=>'text-align:center'),
				),	
				'nama',
				/*'no_urut',*/
				/* 'alamat', */
				'npwr',
				'uraian_retribusi',
				array(
					'name'=>'tanggal_retribusi',
					'type'=>'raw',
					'value'=>'Helper::getTanggal($data->tanggal_retribusi)',
					),
				/*'no_rekening',*/
				array(
					'name'=>'jumlah_keseluruhan',
					'type'=>'raw',
					'value'=>'Helper::Rp($data->jumlah_keseluruhan)',
					),
				/*
				'jumlah_sanksi_bunga',
				'jumlah_sanksi_kenaikan',
				'gross_tonnage',
				*/
				array(
					'class'=>'booster.widgets.TbButtonColumn',
				),
			),
		)); ?>

	</div>
</div>