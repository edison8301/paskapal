<?php
/* @var $this RetribusiController */
/* @var $data Retribusi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_urut')); ?>:</b>
	<?php echo CHtml::encode($data->no_urut); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('npwr')); ?>:</b>
	<?php echo CHtml::encode($data->npwr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_rekening')); ?>:</b>
	<?php echo CHtml::encode($data->no_rekening); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uraian_retribusi')); ?>:</b>
	<?php echo CHtml::encode($data->uraian_retribusi); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_sanksi_bunga')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_sanksi_bunga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_sanksi_kenaikan')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_sanksi_kenaikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_keseluruhan')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_keseluruhan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gross_tonnage')); ?>:</b>
	<?php echo CHtml::encode($data->gross_tonnage); ?>
	<br />

	*/ ?>

</div>