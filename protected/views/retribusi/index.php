<?php
/* @var $this RetribusiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Retribusis',
);

$this->menu=array(
	array('label'=>'Create Retribusi', 'url'=>array('create')),
	array('label'=>'Manage Retribusi', 'url'=>array('admin')),
);
?>

<h1>Retribusis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
