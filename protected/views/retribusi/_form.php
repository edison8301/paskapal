<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'kapal-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Form Retribusi
		</h3>
	</div>
	<div class="box-body">
		<p class="help-block">Fields with <span class="required">*</span> are required.</p>
		<?php echo $form->errorSummary($model); ?>
		
		<?php echo $form->textFieldGroup($model,'nama',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array('maxlength'=>'255')
				)
		)); ?>
		<?php echo $form->textFieldGroup($model,'no_urut',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('maxlength'=>'10')
				)
		)); ?>
		<?php echo $form->textAreaGroup($model,'alamat',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array()
				)
		)); ?>
		<?php echo $form->textFieldGroup($model,'npwr',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('maxlength'=>'255')
				)
		)); ?>
		<?php echo $form->datePickerGroup($model,'tanggal_jatuh_tempo',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true
					)
				),
				'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
		)); ?>
		<?php echo $form->datePickerGroup($model,'tanggal_retribusi',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true
					)
				),
				'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
		)); ?>
		<?php echo $form->datePickerGroup($model,'masa_berlaku_uji',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true
					)
				),
				'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
		)); ?>
		<?php echo $form->datePickerGroup($model,'sampai_dengan_tanggal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true
					)
				),
				'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
		)); ?>
		<?php echo $form->select2Group($model,'uraian_retribusi',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'data'=>RetribusiUraian::getSelect2List(),
				'htmlOptions'=>array('class'=>'span5','empty'=>'- Uraian Retribusi -'),
			))
		); ?>
		<?php echo $form->select2Group($model,'jumlah_ketetapan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'data'=>RetribusiJumlahKetetapan::getSelect2List(),
				'htmlOptions'=>array('class'=>'span5','empty'=>'- Ketetapan Retribusi -'),
			))
		); ?>
		<?php echo $form->textFieldGroup($model,'jumlah_sanksi_bunga',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'prepend'=>'Rp',
				'widgetOptions'=>array(
					'htmlOptions'=>array('maxlength'=>'20','class'=>'format-money')
				)
		)); ?>
		<?php echo $form->textFieldGroup($model,'jumlah_sanksi_kenaikan',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'prepend'=>'Rp',
				'widgetOptions'=>array(
					'htmlOptions'=>array('maxlength'=>'20','class'=>'format-money')
				)
		)); ?>
		<?php echo $form->textFieldGroup($model,'jumlah_keseluruhan',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'prepend'=>'Rp',
				'widgetOptions'=>array(
					'htmlOptions'=>array('maxlength'=>'20','class'=>'format-money')
				)
				
		)); ?>
		<?php echo $form->textFieldGroup($model,'gross_tonnage',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('maxlength'=>'11')
				)
		)); ?>
	</div>
	<div class="box-footer with-border">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-3">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'icon'=>'ok',
					'label'=>'Simpan',
				)); ?>
			</div>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>