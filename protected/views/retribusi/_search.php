<?php
/* @var $this RetribusiController */
/* @var $model Retribusi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_urut'); ?>
		<?php echo $form->textField($model,'no_urut'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alamat'); ?>
		<?php echo $form->textArea($model,'alamat',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'npwr'); ?>
		<?php echo $form->textField($model,'npwr',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_rekening'); ?>
		<?php echo $form->textField($model,'no_rekening',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uraian_retribusi'); ?>
		<?php echo $form->textField($model,'uraian_retribusi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jumlah_sanksi_bunga'); ?>
		<?php echo $form->textField($model,'jumlah_sanksi_bunga'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jumlah_sanksi_kenaikan'); ?>
		<?php echo $form->textField($model,'jumlah_sanksi_kenaikan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jumlah_keseluruhan'); ?>
		<?php echo $form->textField($model,'jumlah_keseluruhan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gross_tonnage'); ?>
		<?php echo $form->textField($model,'gross_tonnage',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->