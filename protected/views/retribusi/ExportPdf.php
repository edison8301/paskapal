<style>
	table {
		border-collapse: collapse;
	}

	.tab-wrap {
		border: 2px solid black;
	}

	.header-info td {
		text-transform: uppercase;
	}

	.table-retribusi th {
		padding:5px;
	}
	
	.table-retribusi td {
		padding: 3px;
	}

	.tabel-bawah table tr {
		padding: 5px;
	}
</style>

<div class="tab-wrap">
<table width="100%" border="1" cellspacing="0" cellpadding="5">
	<tr>
		<th align="left" width="50%" height="40px">PEMERINTAHAN KABUPATEN BELITUNG TIMUR</th>
		<th align="center" width="35%" height="40px">SURAT KETETAPAN RETRIBUSI <br> (SKR)</th>
		<th align="center" colspan="2" width="15%" height="40px">NO URUT <br> 2</th>
	</tr>
</table>
<div>&nbsp;</div>
<table border="" width="100%">
	<tr>
		<td align="right" valign="middle"><i>MASA BERLAKU UJI</i></td>
		<td align="justify" valign="middle">: <?php print Helper::getTanggal($model->masa_berlaku_uji) ?></td>
		<td align="right" valign="middle"><i>SAMPAI DENGAN</i></td>
		<td align="justify" valign="middle">: <?php print Helper::getTanggal($model->sampai_dengan_tanggal) ?></td>
	</tr>
</table>
<div>&nbsp;</div>
<table width="100%" class="header-info">
	<tr>
		<td width="30%"></td>
		<td width="3%"></td>
		<td width="67"></td>
	</tr>
	<tr>
		<td  style="padding-left: 40px;">NAMA</td>
		<td>:</td>
		<td> <?php print $model->nama ?></td>
	</tr>
	<tr>
		<td  style="padding-left: 40px;">ALAMAT</td>
		<td>:</td>
		<td> <?php print $model->alamat ?></td>
	</tr>
	<tr>
		<td  style="padding-left: 40px;"></td>
		<td>:</td>
		<td></td>
		
	</tr>	
	<tr>
		<td  style="padding-left: 40px;">NO POKOK WAJIB RETRIBUSI</td>
		<td>:</td>
		<td> <?php print $model->npwr ?></td>
	</tr>
	<tr>
		<td  style="padding-left: 40px;text-transform:none !important;">Tanggal Jatuh Tempo</td>
		<td>:</td>
		<td> <?php print Helper::getTanggal($model->tanggal_jatuh_tempo) ?></td>
	</tr>				
</table>
<div>&nbsp;</div>
<table class="table-retribusi" width="100%" border="1" cellspacing="0" cellpadding="">
	<tr>
		<th width="5%">NO</th>
		<th colspan="11" width="40%">KODE REKENING</th>
		<th width="45%">URAIAN RETRIBUSI</th>
		<th width="20%"></th>
	</tr>
	<tr>
		<?php
			$norek = Pengaturan::getPengaturan('retribusi_kode_rekening'); 
		 	$array = str_split($norek);
		?>
		<td style="text-align:center;">1</td>
		<td style="text-align:center;"><?php print($array[0]) ?></td>
		<td style="text-align:center;"><?php print($array[1]) ?></td>
		<td style="text-align:center;"><?php print($array[2]) ?></td>
		<td style="text-align:center;"><?php print($array[3]) ?></td>
		<td style="text-align:center;"><?php print($array[4]) ?></td>
		<td style="text-align:center;"><?php print($array[5]) ?></td>
		<td style="text-align:center;"><?php print($array[6]) ?></td>
		<td style="text-align:center;"><?php print($array[7]) ?></td>
		<td style="text-align:center;"><?php print($array[8]) ?></td>
		<td style="text-align:center;"><?php print($array[9]) ?></td>
		<td style="text-align:center;"><?php print($array[10]) ?></td>
		<td rowspan="4"><p>Retribusi Pengujian Kendaraan Bermotor</p>
		&nbsp; <p style="font-weight: bold;"><?php print $model->uraian_retribusi; ?></td>
		<td class="" rowspan="4" width="10%"  style="font-weight: bold;"><?php print Helper::rp($model->jumlah_ketetapan) ?></td>
	</tr>
	<tr>
		<td style="text-align:center;">2</td>
		<td></td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
	</tr>
	<tr>
		<td style="text-align:center;">3</td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
	</tr>
	<tr>
		<td style="text-align:center;">4</td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
	</tr>
	<tr>
		<td rowspan="5" colspan="12" >&nbsp;</td>
		<td style="border-bottom:none;">Jumlah Ketetapan Retribusi</td>
		<td style="border-bottom:none;">&nbsp; </td>
	</tr>
	<tr>
		<td style="border-top:none;border-bottom:none;">Jumlah sanksi : &nbsp; a. Bunga</td>
		<td style="border-top:none;border-bottom:none;">&nbsp; <?php print Helper::rp($model->jumlah_sanksi_bunga) ?></td>
	</tr>
	<tr>
		<td style="border-top:none;padding-left: 102px;">b. Kenaikan</td>
		<td style="border-top:none;">&nbsp; <?php print Helper::rp($model->jumlah_sanksi_kenaikan) ?></td>
	</tr>
	<tr>
		<td class="" style="text-align: left;" valign="middle">Jumlah Keseluruhan</td>
		<td class="" style="font-weight: bold;"><?php print Helper::rp($model->jumlah_keseluruhan); ?></td>
	</tr>

</table>

<table width="100%" style="margin-left:10px;">
	<tr>
		<td width="20%"><b><i>Dengan Huruf :</i></b></td>
		<td width="80%" style="text-transform:uppercase;"><i><?php print Helper::getTerbilang($model->jumlah_keseluruhan,'') ?> rupiah</i></td>
	</tr>
	<tr>
		<td><b>PERHATIAN :</b></td>
		<td></td>
	</tr>
</table>
<ol style="margin-left: 10px;padding-left:20px">
	<li>Harap Penyetoran dilakukan di Bank/ Bendahara Perencanaan dinas Perhubungan Kabupaten Belitung Timur.</li>
	<li>Apabila SKR ini tidak atau kurang dibayar lewat dibayar lewat waktu paling lama 1 (Satu) hari setelah SKR diterima atau ( Tanggal Jatuh Tempo ) dikenakan sangsi berupa bunga sebesar 2% per-Bulan.</li>
</ol>
<hr/>
<table align="right" border="0" cellspacing="0" style="margin-right:80px;" >
	<tr>
		<td style="text-align: center;">Manggar, <?php print Helper::getTanggal($model->tanggal_retribusi) ?> <br> &nbsp;</td>
	</tr>
	<tr>
		<td style="text-align: center;">KEPALA DINAS PERHUBUNGAN</td>
	</tr>
	<tr>
		<td style="text-align: center;">KABUPATEN BELITUNG TIMUR</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="text-align: center;"><?php print Pengaturan::getPengaturan('penandatangan_nama') ?></td>
	</tr>
	<tr>
		<td style="text-align: center;">NIP. <?php print Pengaturan::getPengaturan('penandatangan_nip') ?></td>
	</tr>
</table>
<hr/>
<div style="width: 100%; height: auto; float: left">
	<div style="width: 69%; height: auto; float: left">
		<table width="100%" style="margin-left:10px;" cellpadding="5" border="">
			<tr>
				<td style="font-weight: bold;">TANDA PAS</td>
				<td width="5%">:</td>
				<td width="30%" style="border: 1px solid black; text-align:center;" celpadding="3">A13 NO. 755</td>
				<td>&nbsp;</td>
			</tr>
		<!-- </table>
		<table width="100%" style="margin-left:10px;" cellpadding="5" border=""> -->
			<tr>
				<td style="font-weight: bold;">Nama</td>
				<td>:</td>
				<td colspan="2" style="font-weight: bold;"><?php print $model->nama ?></td>
			</tr>
			<tr>
				<td>ALAMAT</td>
				<td>:</td>
				<td colspan="2"><?php print $model->alamat ?></td>
			</tr>
			<tr>
				<td style="font-weight: bold;">NPWR</td>
				<td>:</td>
				<td colspan="2"><?php print $model->npwr ?></td>
			</tr>
			<tr>
				<td style="font-weight: bold;">URAIAN RETRIBUSI</td>
				<td>:</td>
				<td colspan="2"><?php print $model->uraian_retribusi ?></td>
			</tr>
			<tr>
				<td style="font-weight: bold;">GROSS TONNAGE</td>
				<td>:</td>
				<td colspan="2" style="font-weight: bold;"><?php print $model->gross_tonnage ?></td>
			</tr>
			<tr>
				<td style="font-weight: bold;">Total</td>
				<td>:</td>
				<td colspan="2" style="font-weight: bold;"><?php print Helper::rp($model->jumlah_keseluruhan); ?></td>
			</tr>
			<tr>
				<td style="font-weight: bold;">Terbilang</td>
				<td>:</td>
				<td colspan="2" style="font-weight: bold;text-transform:uppercase;"><?php print Helper::getTerbilang($model->jumlah_keseluruhan,'') ?> RUPIAH</td>
			</tr>

		</table>
	</div>
	<div style="width: 30%; height: auto; float: right">
		<table border="" width="100%" style="margin-right:10px;" class="tabel-bawah">
			<tr>
				<td style="font-weight: bold;" align="center" valign="middle">NO. URUT</td>
			</tr>
			<tr>
				<td align="center" valign="middle"><?php print $model->no_urut ?></td>
			</tr>
			<tr>
				<td style="padding-left:35px;padding-top: 35px;" valign="middle" align="left"> Manggar, <?php print Helper::getTanggal($model->tanggal_retribusi) ?></td>
			</tr>
			<tr>
				<td valign="middle" align="center">YANG MENERIMA</td>
			</tr>
			<tr>
				<td style="padding-top: 70px;"><hr/></td>
			</tr>
		</table>
	</div>
</div>
</div>