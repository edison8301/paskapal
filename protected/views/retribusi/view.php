<?php
$this->breadcrumbs=array(
	'Retribusi'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Retribusi', 'url'=>array('index')),
	array('label'=>'Create Retribusi', 'url'=>array('create')),
	array('label'=>'Update Retribusi', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Retribusi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Retribusi', 'url'=>array('admin')),
);
?>

<h1>Tampil Retribusi </h1>
<div>&nbsp;</div>

	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'link',
		'url'=>array('update','id'=>$model->id),
		'context'=>'primary',
		'icon'=>'pencil',
		'label'=>'Sunting',
		'htmlOptions'=>array('class'=>'dim')
	)); ?>&nbsp;
	<?php 
		$this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'link',
					'context'=>'success',
					'icon'=>'print',
					'url'=>array('retribusi/exportPdf','id'=>$model->id),
					'label'=>'Cetak SKR',
					'htmlOptions'=>array('target'=>'_blank'),
			));
	 ?>

<div>&nbsp;</div>
<?php $this->widget('booster.widgets.TbDetailView', array(
	'data'=>$model,
	'type'=>'striped bordered condensed',
	'attributes'=>array(
		'nama',
		'no_urut',
		'alamat',
		'npwr',
		array(
			'label'=>'Tanggal Jatuh Tempo',
			'type'=>'raw',
			'value'=>Helper::getTanggal($model->tanggal_jatuh_tempo)
		),	
		array(
			'label'=>'Tanggal Retribusi',
			'type'=>'raw',
			'value'=>Helper::getTanggal($model->tanggal_retribusi)
		),	
		array(
			'label'=>'Masa Berlaku Uji',
			'type'=>'raw',
			'value'=>Helper::getTanggal($model->masa_berlaku_uji)
		),	
		array(
			'label'=>'Batas Masa Berlaku',
			'type'=>'raw',
			'value'=>Helper::getTanggal($model->sampai_dengan_tanggal)
		),			
		'uraian_retribusi',
		array(
			'label'=>'Jumlah Ketetapan Retribusi',
			'type'=>'raw',
			'value'=>Helper::rp($model->jumlah_ketetapan)
		),	
		array(
			'label'=>'Jumlah Sanksi Bunga',
			'type'=>'raw',
			'value'=>Helper::rp($model->jumlah_sanksi_bunga)
		),	
		array(
			'label'=>'Jumlah Sanksi Kenaikan',
			'type'=>'raw',
			'value'=>Helper::rp($model->jumlah_sanksi_kenaikan)
		),	
		array(
			'label'=>'Jumlah Keseluruhan',
			'type'=>'raw',
			'value'=>Helper::rp($model->jumlah_keseluruhan),
			'htmlOptions'=>array('style'=>'text-align: right !important;'),
		),	
		'gross_tonnage',
	),
)); ?>


<div>&nbsp;</div>

