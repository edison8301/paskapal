<?php
$this->breadcrumbs=array(
	'Retribusi Uraians'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List RetribusiUraian','url'=>array('index')),
array('label'=>'Manage RetribusiUraian','url'=>array('admin')),
);
?>

<h1>Create RetribusiUraian</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>