<?php
$this->breadcrumbs=array(
	'Retribusi Uraians'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List RetribusiUraian','url'=>array('index')),
	array('label'=>'Create RetribusiUraian','url'=>array('create')),
	array('label'=>'View RetribusiUraian','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage RetribusiUraian','url'=>array('admin')),
	);
	?>

	<h1>Update RetribusiUraian <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>