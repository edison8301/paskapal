<?php
$this->breadcrumbs=array(
	'Retribusi Uraians'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List RetribusiUraian','url'=>array('index')),
array('label'=>'Create RetribusiUraian','url'=>array('create')),
array('label'=>'Update RetribusiUraian','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete RetribusiUraian','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage RetribusiUraian','url'=>array('admin')),
);
?>

<h1>View RetribusiUraian #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'kode',
		'nilai',
),
)); ?>
