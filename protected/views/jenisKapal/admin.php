<?php
$this->breadcrumbs=array(
	'Jenis Kapal'=>array('admin'),
	'Kelola',
);
?>

<div class="box box-success">
	<div class="box-header with-border">
		<?php 
			$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array(),
			'url'=>array('create'),
			'context'=>'success',
			'icon'=>'plus',
			'label'=>'Tambah',
		)); ?>&nbsp;
	</div>
	<div class="box-body">
		<?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'jenis-kapal-grid',
			'type'=>'striped bordered condensed',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
					array(
					'header'=>'No',
					'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
					'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
					'htmlOptions'=>array('style'=>'text-align:center'),
					),	
					'nama',
			array(
			'class'=>'booster.widgets.TbButtonColumn',
			'template'=>'{update},{delete}'
			),
			),
		));?>
	</div>
</div>