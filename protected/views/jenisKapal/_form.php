<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'type'=>'horizontal',
	'id'=>'jenis-kapal-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>
<div class="well">
	<?php echo $form->textFieldGroup($model,'nama',array(
	'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
	'widgetOptions'=>
		array(
		'htmlOptions'=>array(
			'maxlength'=>255
			))
	)); ?>
</div>
<div class="form-actions well">
	<div class="row">
	 	<div class="col-sm-3"></div>
	 	<div class="col-sm-9">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>'Simpan',
				)); ?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>
