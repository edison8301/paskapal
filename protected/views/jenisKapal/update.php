<?php
$this->breadcrumbs=array(
	'Jenis Kapal'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Sunting',
);

	$this->menu=array(
	array('label'=>'List JenisKapal','url'=>array('index')),
	array('label'=>'Create JenisKapal','url'=>array('create')),
	array('label'=>'View JenisKapal','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage JenisKapal','url'=>array('admin')),
	);
	?>

	<h1>Sunting Data Jenis Kapal</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>