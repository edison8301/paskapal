<?php
$this->breadcrumbs=array(
	'Jenis Kapal'=>array('index'),
	'Tambah',
);

$this->menu=array(
array('label'=>'List JenisKapal','url'=>array('index')),
array('label'=>'Manage JenisKapal','url'=>array('admin')),
);
?>

<h1>Tambah Data Jenis Kapal</h1>
<div>&nbsp;</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>