<?php
$this->breadcrumbs=array(
	'Jenis Kapals'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List JenisKapal','url'=>array('index')),
array('label'=>'Create JenisKapal','url'=>array('create')),
array('label'=>'Update JenisKapal','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete JenisKapal','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage JenisKapal','url'=>array('admin')),
);
?>

<h1>View JenisKapal #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
