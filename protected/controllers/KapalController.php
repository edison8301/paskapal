<?php

class KapalController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array(),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('index','view','exportExcel','admin','delete','create','update',),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array(),
'users'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Kapal;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Kapal']))
{
$model->attributes=$_POST['Kapal'];
if($model->save())
$this->redirect(array('admin'));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Kapal']))
{
$model->attributes=$_POST['Kapal'];
if($model->save())
$this->redirect(array('admin'));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Kapal');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Kapal('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Kapal']))
$model->attributes=$_GET['Kapal'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Kapal::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

  public function actionExportExcel()
	{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));

		$PHPExcel = new PHPExcel();
			
		$PHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Kapal');
		
		$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nama Kapal');
		$PHPExcel->getActiveSheet()->setCellValue('C3', 'Tanda Pas');
		$PHPExcel->getActiveSheet()->setCellValue('D3', 'Nama Pemilik');
		$PHPExcel->getActiveSheet()->setCellValue('E3', 'Alamat Pemilik');
		$PHPExcel->getActiveSheet()->setCellValue('F3', 'Npwp Pemilik');
		$PHPExcel->getActiveSheet()->setCellValue('G3', 'Ukuran Panjang');
		$PHPExcel->getActiveSheet()->setCellValue('H3', 'Ukuran Lebar');
		$PHPExcel->getActiveSheet()->setCellValue('I3', 'Ukuran D');
		$PHPExcel->getActiveSheet()->setCellValue('J3', 'Tonase Kotor');
		$PHPExcel->getActiveSheet()->setCellValue('K3', 'Tonase Bersih');
		$PHPExcel->getActiveSheet()->setCellValue('L3', 'Tempat Pembangunan');
		$PHPExcel->getActiveSheet()->setCellValue('M3', 'Tahun Pembangunan');
		$PHPExcel->getActiveSheet()->setCellValue('N3', 'Bahan Utama');
		$PHPExcel->getActiveSheet()->setCellValue('O3', 'Jumlah Geladak');
		$PHPExcel->getActiveSheet()->setCellValue('P3', 'Penggerak');
		$PHPExcel->getActiveSheet()->setCellValue('Q3', 'Mesin Merek');
		$PHPExcel->getActiveSheet()->setCellValue('R3', 'Mesin Daya');
		
		$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
		$PHPExcel->getActiveSheet()->getStyle('A3:R3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A3:R3')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);		
		$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
		$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(25);
		
		$i = 1;
		$kolom = 4;

		foreach(Kapal::model()->findAll() as $data)
		{
			$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nama_kapal);
			$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->tanda_pas);
			$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->nama_pemilik);
			$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->alamat_pemilik);
			$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->npwp_pemilik);
			$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->ukuran_p);
			$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->ukuran_l);
			$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->ukuran_d);
			$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->tonase_kotor);
			$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->tonase_bersih);
			$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->tempat_pembangunan);
			$PHPExcel->getActiveSheet()->setCellValue('M'.$kolom, $data->tahun_pembangunan);
			$PHPExcel->getActiveSheet()->setCellValue('N'.$kolom, $data->bahan_utama);
			$PHPExcel->getActiveSheet()->setCellValue('O'.$kolom, $data->jumlah_geladak);
			$PHPExcel->getActiveSheet()->setCellValue('P'.$kolom, $data->penggerak);
			$PHPExcel->getActiveSheet()->setCellValue('Q'.$kolom, $data->mesin_merek);
			$PHPExcel->getActiveSheet()->setCellValue('R'.$kolom, $data->mesin_daya);
										
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':R'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$i++; $kolom++;
		}
	
		$filename = time().'_LaporanKapal.xlsx';

			$path = Yii::app()->basePath.'/../exports/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);

	}



/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='kapal-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
