<?php

class RetribusiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','exportExcel','create','update','exportPdf','export','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Retribusi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Retribusi']))
		{
			$model->attributes=$_POST['Retribusi'];

			$model->jumlah_sanksi_bunga = str_replace(".", "", $model->jumlah_sanksi_bunga);
			$model->jumlah_sanksi_kenaikan = str_replace(".", "", $model->jumlah_sanksi_kenaikan);
			$model->jumlah_keseluruhan = str_replace(".", "", $model->jumlah_keseluruhan);

			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$model->jumlah_sanksi_bunga = number_format($model->jumlah_sanksi_bunga,0,0,'.');
		$model->jumlah_sanksi_kenaikan = number_format($model->jumlah_sanksi_kenaikan,0,0,'.');
		$model->jumlah_keseluruhan = number_format($model->jumlah_keseluruhan,0,0,'.');

		if(isset($_POST['Retribusi']))
		{
			$model->attributes=$_POST['Retribusi'];

			$model->jumlah_sanksi_bunga = str_replace(".", "", $model->jumlah_sanksi_bunga);
			$model->jumlah_sanksi_kenaikan = str_replace(".", "", $model->jumlah_sanksi_kenaikan);
			$model->jumlah_keseluruhan = str_replace(".", "", $model->jumlah_keseluruhan);

			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Retribusi');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Retribusi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Retribusi']))
			$model->attributes=$_GET['Retribusi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Retribusi the loaded model
	 * @throws CHttpException
	 */

	public function actionExportPdf($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 10;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$mpdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);
		$retribusi = Retribusi::model()->findByPk($id);

		$html = $this->renderPartial('exportPdf',array('model'=>$model,'retribusi'=>$retribusi),true);

		$mpdf->WriteHTML($html);

		$mpdf->Output();			
	}		

	 public function actionExportExcel()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));

		$criteria = new CDbCriteria;
			$params = array();

			if ($_GET['tanggal_jenis']==1) {
				
				if(!empty($_GET['tanggal_awal'])) {
				$criteria->addCondition('tanggal_retribusi >= :tanggal_awal');
				$params[':tanggal_awal'] = $_GET['tanggal_awal'];
				}

				if(!empty($_GET['tanggal_akhir'])) {
					$criteria->addCondition('tanggal_retribusi <= :tanggal_akhir');
					$params[':tanggal_akhir'] = $_GET['tanggal_akhir'];
				}			

				$criteria->params = $params;
				$criteria->order = 'tanggal_retribusi ASC';
				$header = 'Laporan Retribusi Berdasarkan Tanggal Retribusi ('.$_GET['tanggal_awal'].' sampai '.$_GET['tanggal_akhir'].')';

			}

			if ($_GET['tanggal_jenis']==2) {
				
				if(!empty($_GET['tanggal_awal'])) {
				$criteria->addCondition('tanggal_jatuh_tempo >= :tanggal_awal');
				$params[':tanggal_awal'] = $_GET['tanggal_awal'];
				}

				if(!empty($_GET['tanggal_akhir'])) {
					$criteria->addCondition('tanggal_jatuh_tempo <= :tanggal_akhir');
					$params[':tanggal_akhir'] = $_GET['tanggal_akhir'];
				}			

				$criteria->params = $params;
				$criteria->order = 'tanggal_jatuh_tempo ASC';

				$header = 'Laporan Retribusi Berdasarkan Tanggal Jatuh Tempo ('.$_GET['tanggal_awal'].' sampai '.$_GET['tanggal_akhir'].')';
				
			}

			if ($_GET['tanggal_jenis']==3) {
				
				if(!empty($_GET['tanggal_awal'])) {
				$criteria->addCondition('masa_berlaku_uji >= :tanggal_awal');
				$params[':tanggal_awal'] = $_GET['tanggal_awal'];
				}

				if(!empty($_GET['tanggal_akhir'])) {
					$criteria->addCondition('masa_berlaku_uji <= :tanggal_akhir');
					$params[':tanggal_akhir'] = $_GET['tanggal_akhir'];
				}			

				$criteria->params = $params;
				$criteria->order = 'masa_berlaku_uji ASC';

				$header = 'Laporan Retribusi Berdasarkan Tanggal Masa Berlaku Uji ('.$_GET['tanggal_awal'].' sampai '.$_GET['tanggal_akhir'].')';
				
			}

			if ($_GET['tanggal_jenis']==4) {
				
				if(!empty($_GET['tanggal_awal'])) {
				$criteria->addCondition('sampai_dengan_tanggal >= :tanggal_awal');
				$params[':tanggal_awal'] = $_GET['tanggal_awal'];
				}

				if(!empty($_GET['tanggal_akhir'])) {
					$criteria->addCondition('sampai_dengan_tanggal <= :tanggal_akhir');
					$params[':tanggal_akhir'] = $_GET['tanggal_akhir'];
				}			

				$criteria->params = $params;
				$criteria->order = 'sampai_dengan_tanggal ASC';

				$header = 'Laporan Retribusi Berdasarkan Tanggal Batas Masa Berlaku Uji ('.$_GET['tanggal_awal'].' sampai '.$_GET['tanggal_akhir'].')';
				
			}

		$PHPExcel = new PHPExcel();
			
		$PHPExcel->getActiveSheet()->setCellValue('A1', $header);
		
		$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
		$PHPExcel->getActiveSheet()->setCellValue('C3', 'No Urut');
		$PHPExcel->getActiveSheet()->setCellValue('D3', 'Alamat');
		$PHPExcel->getActiveSheet()->setCellValue('E3', 'NPWR');
		$PHPExcel->getActiveSheet()->setCellValue('F3', 'Tanggal Jatuh Tempo');
		$PHPExcel->getActiveSheet()->setCellValue('G3', 'Tanggal Retribusi');
		$PHPExcel->getActiveSheet()->setCellValue('H3', 'Masa Berlaku Uji');
		$PHPExcel->getActiveSheet()->setCellValue('I3', 'Sampai Dengan Tanggal');
		$PHPExcel->getActiveSheet()->setCellValue('J3', 'Uraian Retribusi');
		$PHPExcel->getActiveSheet()->setCellValue('K3', 'Jumlah Sanksi Bunga');
		$PHPExcel->getActiveSheet()->setCellValue('L3', 'Jumlah Sanksi Kenaikan');
		$PHPExcel->getActiveSheet()->setCellValue('M3', 'Jumlah Keseluruhan');
		$PHPExcel->getActiveSheet()->setCellValue('N3', 'Gross Tonage');
		
		$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
		$PHPExcel->getActiveSheet()->getStyle('A3:n3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A3:N3')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A3:N3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);		
		$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
		$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
		
		$i = 1;
		$kolom = 4;

		foreach(Retribusi::model()->findAll($criteria) as $data)
		{
			$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nama);
			$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->no_urut);
			$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->alamat);
			$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->npwr);
			$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->tanggal_jatuh_tempo);
			$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->tanggal_retribusi);
			$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->masa_berlaku_uji);
			$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->sampai_dengan_tanggal);
			$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->uraian_retribusi);
			$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->jumlah_sanksi_bunga);
			$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->jumlah_sanksi_kenaikan);
			$PHPExcel->getActiveSheet()->setCellValue('M'.$kolom, $data->jumlah_keseluruhan);
			$PHPExcel->getActiveSheet()->setCellValue('N'.$kolom, $data->gross_tonnage);
										
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':N'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$i++; $kolom++;
		}
	
		$filename = time().'_LaporanRetribusi.xlsx';

			$path = Yii::app()->basePath.'/../exports/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);

	}

	public function loadModel($id)
	{
		$model=Retribusi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Retribusi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='retribusi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionExport()
	{	
		$model = new ExportRetribusiForm;

			if(isset($_POST['ExportRetribusiForm']))
			{
				$model->attributes = $_POST['ExportRetribusiForm'];

				if($model->validate())
				{
					$this->redirect(array('exportExcel','tanggal_jenis'=>$_POST['ExportRetribusiForm']['berdasarkan_tanggal'],'tanggal_awal'=>$_POST['ExportRetribusiForm']['tanggal_awal'],'tanggal_akhir'=>$_POST['ExportRetribusiForm']['tanggal_akhir']));
				}
			}

		$this->render('export',array(
			'model'=>$model,
		));

	}

}
