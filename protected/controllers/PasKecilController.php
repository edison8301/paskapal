<?php

class PasKecilController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index','view','exportExcel','exportPdf','exportPdf2','registerPasKecilPdf','export'),
			'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('delete','create','update'),
			'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
			'actions'=>array('admin'),
			'users'=>array('@'),
			),
			array('deny',  // deny all users
			'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$paskecil = $this->loadModel($id);
		$id_kapal = $paskecil->id_kapal;
		if ($id_kapal == 0){
			$id_kapal = 105;
		}
		

		$kapal = Kapal::model()->findByPk($id_kapal);
		$this->render('view',array(
			'paskecil'=>$this->loadModel($id),
			'kapal' => $kapal,
		));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{

		$model=new PasKecil;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PasKecil']))
		{
			$model->attributes=$_POST['PasKecil'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['PasKecil']))
{
$model->attributes=$_POST['PasKecil'];
if($model->save())
$this->redirect(array('admin'));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('PasKecil');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new PasKecil('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PasKecil']))
			$model->attributes=$_GET['PasKecil'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/

	public function actionExportPdf($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 10;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$mpdf = new mPDF('UTF-8','A4',9,'',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		
		$paskecil = $this->loadModel($id);
		$kapal = Kapal::model()->findByAttributes(array('id'=>$paskecil->id_kapal));
		
		$html = $this->renderPartial('exportPdf',array('paskecil'=>$paskecil,'kapal'=>$kapal),true);

		$mpdf->WriteHTML($html);

		$mpdf->Output();	

	}

	public function actionExportPdf2($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 10;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$mpdf = new mPDF(
			'UTF-8',
			'A4',
			9,
			'',
			$marginLeft,
			$marginRight,
			$marginTop,
			$marginBottom,
			$marginHeader,
			$marginFooter);

		
		$paskecil = $this->loadModel($id);
		$kapal = Kapal::model()->findByAttributes(array('id'=>$paskecil->id_kapal));
		/*$nama = Pengaturan::getPengaturan('penandatangan_nama'); */
		
		$html = $this->renderPartial('exportPdf2',
			array(
				'paskecil'=>$paskecil,
				'kapal'=>$kapal),
				true);

		$mpdf->WriteHTML($html);

		$mpdf->Output();	

	}

	public function actionRegisterPasKecilPdf($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 10;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$mpdf = new mPDF('UTF-8','A4',9,'',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$paskecil = PasKecil::model()->findByPk($id);
		

		$html = $this->renderPartial('registerPasKecilPdf',array('paskecil'=>$paskecil),true);

		$mpdf->WriteHTML($html);

		$mpdf->Output();			
	}	

	public function actionExport()
	{	
		$model = new ExportPasKecilForm;

			if(isset($_POST['ExportPasKecilForm']))
			{
				$model->attributes = $_POST['ExportPasKecilForm'];

				if($model->validate())
				{
					$this->redirect(array('exportExcel','tanggal_jenis'=>$_POST['ExportPasKecilForm']['berdasarkan_tanggal'],'tanggal_awal'=>$_POST['ExportPasKecilForm']['tanggal_awal'],'tanggal_akhir'=>$_POST['ExportPasKecilForm']['tanggal_akhir']));
				}
			}

		$this->render('export',array(
			'model'=>$model,
		));
	}

	public function actionExportExcel()
		{
			if(empty($_GET['tanggal_awal']) || empty($_GET['tanggal_akhir']))
				$this->redirect(Yii::app()->request->urlReferrer);


			spl_autoload_unregister(array('YiiBase','autoload'));
			
			Yii::import('application.vendors.PHPExcel',true);
			
			spl_autoload_register(array('YiiBase', 'autoload'));

			$criteria = new CDbCriteria;
			$params = array();

			if ($_GET['tanggal_jenis']==1) {
				
				if(!empty($_GET['tanggal_awal'])) {
				$criteria->addCondition('tanggal_pemberian >= :tanggal_awal');
				$params[':tanggal_awal'] = $_GET['tanggal_awal'];
				}

				if(!empty($_GET['tanggal_akhir'])) {
					$criteria->addCondition('tanggal_pemberian <= :tanggal_akhir');
					$params[':tanggal_akhir'] = $_GET['tanggal_akhir'];
				}			

				$criteria->params = $params;
				$criteria->order = 'tanggal_pemberian ASC';
				$header = 'Laporan Pas-Kecil Berdasarkan Tanggal Pemberian ('.$_GET['tanggal_awal'].' sampai '.$_GET['tanggal_akhir'].')';

			}

			if ($_GET['tanggal_jenis']==2) {
				
				if(!empty($_GET['tanggal_awal'])) {
				$criteria->addCondition('tanggal_batas_berlaku >= :tanggal_awal');
				$params[':tanggal_awal'] = $_GET['tanggal_awal'];
				}

				if(!empty($_GET['tanggal_akhir'])) {
					$criteria->addCondition('tanggal_batas_berlaku <= :tanggal_akhir');
					$params[':tanggal_akhir'] = $_GET['tanggal_akhir'];
				}			

				$criteria->params = $params;
				$criteria->order = 'tanggal_batas_berlaku ASC';

				$header = 'Laporan Pas-Kecil Berdasarkan Tanggal Batas Berlaku ('.$_GET['tanggal_awal'].' sampai '.$_GET['tanggal_akhir'].')';
				
			}

			$PHPExcel = new PHPExcel();
				
			$PHPExcel->getActiveSheet()->setCellValue('A1', $header);
			
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nomor');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'Tujuan Penggunaan');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'Tempat Pemberian');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'Tanggal Pemberian');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'Tanggal Batas Berlaku');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'Nama Kapal');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'Nama Pemilik');
			$PHPExcel->getActiveSheet()->setCellValue('I3', 'Alamat Pemilik');
			$PHPExcel->getActiveSheet()->setCellValue('J3', 'Tanda Pas');
			$PHPExcel->getActiveSheet()->setCellValue('K3', 'Gross Tonnage');
			$PHPExcel->getActiveSheet()->setCellValue('L3', 'Net Tonnage');
			$PHPExcel->getActiveSheet()->setCellValue('M3', 'Panjang x Lebar x Dalam Kapal');
			$PHPExcel->getActiveSheet()->setCellValue('N3', 'Penggerak');
			$PHPExcel->getActiveSheet()->setCellValue('O3', 'Merk Mesin');
			$PHPExcel->getActiveSheet()->setCellValue('P3', 'Daya Mesin');
			$PHPExcel->getActiveSheet()->setCellValue('Q3', 'Bahan Utama');
			$PHPExcel->getActiveSheet()->setCellValue('R3', 'Jumlah Geladak');
			$PHPExcel->getActiveSheet()->setCellValue('S3', 'Jumlah Baling-baling');
			$PHPExcel->getActiveSheet()->setCellValue('T3', 'Jenis Kapal');
			$PHPExcel->getActiveSheet()->setCellValue('U3', 'Tempat Pembuatan Kapal');
			$PHPExcel->getActiveSheet()->setCellValue('V3', 'Tahun Pembuatan Kapal');
			$PHPExcel->getActiveSheet()->setCellValue('W3', 'Tempat Register Kapal');
			$PHPExcel->getActiveSheet()->setCellValue('X3', 'Nomor Register Kapal');
			$PHPExcel->getActiveSheet()->setCellValue('Y3', 'Catatan');
			
			$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				
			$PHPExcel->getActiveSheet()->getStyle('A3:Y3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
			$PHPExcel->getActiveSheet()->getStyle('A3:Y3')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle('A3:Y3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);		
			$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
			$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(50);
			$PHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(50);
			
			$i = 1;
			$kolom = 4;

			foreach(PasKecil::model()->findAll($criteria) as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nomor);
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->tujuan_penggunaan);
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->tempat_pemberian);
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->tanggal_pemberian);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->tanggal_batas_berlaku);
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->nama_kapal);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->nama_pemilik);
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->alamat_pemilik);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->tanda_pas);
				$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->gross_tonnage);
				$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->net_tonnage);
				$PHPExcel->getActiveSheet()->setCellValue('M'.$kolom, $data->panjang_kapal.' X '.$data->lebar_kapal.' X '.$data->dalam_kapal);
				$PHPExcel->getActiveSheet()->setCellValue('N'.$kolom, $data->mesin_penggerak);
				$PHPExcel->getActiveSheet()->setCellValue('O'.$kolom, $data->merk_mesin);
				$PHPExcel->getActiveSheet()->setCellValue('P'.$kolom, $data->daya_mesin);
				$PHPExcel->getActiveSheet()->setCellValue('Q'.$kolom, PasKecil::getBahanUtama($data->bahan_utama));
				$PHPExcel->getActiveSheet()->setCellValue('R'.$kolom, $data->jumlah_geladak);
				$PHPExcel->getActiveSheet()->setCellValue('S'.$kolom, $data->jumlah_baling_baling);
				$PHPExcel->getActiveSheet()->setCellValue('T'.$kolom, PasKecil::getJenisKapal($data->jenis_kapal));
				$PHPExcel->getActiveSheet()->setCellValue('U'.$kolom, $data->tempat_pembuatan_kapal);
				$PHPExcel->getActiveSheet()->setCellValue('V'.$kolom, $data->tahun_pembuatan_kapal);
				$PHPExcel->getActiveSheet()->setCellValue('W'.$kolom, $data->tempat_register_kapal);
				$PHPExcel->getActiveSheet()->setCellValue('X'.$kolom, $data->nomor_register_kapal);
				$PHPExcel->getActiveSheet()->setCellValue('Y'.$kolom, $data->catatan);
											
				$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':Y'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

				$PHPExcel->getActiveSheet()->getStyle('E'.$kolom.':F'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


				$i++; $kolom++;
			}
		
			$filename = time().'_LaporanPas-Kecil.xlsx';

				$path = Yii::app()->basePath.'/../exports/';
				$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
				$objWriter->save($path.$filename);	
				$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);

		}


public function loadModel($id)
{
$model=PasKecil::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='pas-kecil-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
