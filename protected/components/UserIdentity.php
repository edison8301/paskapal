<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */

	public function getId()
    {
       return $this->_id;
    }

	public function authenticate()
	{
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		
		if($user!==null)
		{
			if(CPasswordHelper::verifyPassword($this->password, $user->password))
			{
				Yii::app()->session['user_id']=$user->id;
				$this->_id = $user->username;
				$this->errorCode=self::ERROR_NONE;
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}	
		
		}
		return !$this->errorCode;
	}
}