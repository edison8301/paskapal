<?php

class Helper {

	public static function rp($jumlah,$null=null)
	{
		if($jumlah==null)
		{
			return $null;
		} else {
			return 'Rp '.number_format($jumlah,0,',','.');
		}
	}
	
	public static function tanggalSingkat($tanggal=null)
	{
		$output = '';
		
		if($tanggal!=null)
		{
			$tanggal = explode('-',$tanggal);
				
			$output = $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
		}
		
		return $output;
	}

	public static function getCreatedTime($waktu)
	{
		if($waktu == '')
			return null;
		else {
		$time = strtotime($waktu);
		
		$h = date('N',$time);
		
		if($h == '1') $hari = 'Senin';
		if($h == '2') $hari = 'Selasa';
		if($h == '3') $hari = 'Rabu';
		if($h == '4') $hari = 'Kamis';
		if($h == '5') $hari = 'Jumat';
		if($h == '6') $hari = 'Sabtu';
		if($h == '7') $hari = 'Minggu';
		
		
		$tgl = date('j',$time);
		
		$h = date('n',$time);
		
		if($h == '1') $bulan = 'Januari';
		if($h == '2') $bulan = 'Februari';
		if($h == '3') $bulan = 'Maret';
		if($h == '4') $bulan = 'April';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Agustus';
		if($h == '9') $bulan = 'September';
		if($h == '10') $bulan = 'Oktober';
		if($h == '11') $bulan = 'November';
		if($h == '12') $bulan = 'Desember';
		
		$tahun  = date('Y',$time);
		
		$pukul = date('H:i:s',$time);
		
		$output = $hari.', '.$tgl.' '.$bulan.' '.$tahun.' | '.$pukul.' WIB';
		
		return $output;
		}
		
	}	

	public static function getTanggalSingkat($waktu)
	{
		if($waktu=='0000-00-00' OR $waktu == null)
		{
			return null;
		}

		$time = strtotime($waktu);
		
		$h = date('N',$time);
		
		$tgl = date('j',$time);
		
		$h = date('n',$time);
		
		if($h == '1') $bulan = 'Jan';
		if($h == '2') $bulan = 'Feb';
		if($h == '3') $bulan = 'Mar';
		if($h == '4') $bulan = 'Apr';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Aug';
		if($h == '9') $bulan = 'Sep';
		if($h == '10') $bulan = 'Okt';
		if($h == '11') $bulan = 'Nov';
		if($h == '12') $bulan = 'Des';
		
		$tahun  = date('Y',$time);
		
		$pukul = date('h:i:s',$time);
		
		$output = $tgl.' '.$bulan.' '.$tahun;
		
		return $output;
		
	}


	public static function getTanggal($tgl=null)
	{
		if($tgl == '0000-00-00' OR $tgl == null)
			return null;
		else
			return date('j',strtotime($tgl))." ".Helper::getBulan($tgl)." ".date('Y',strtotime($tgl));	
	}

	public static function getBulan($tanggal)
	{
		if($tanggal == '0000-00-00' OR $tanggal == null)
		{
			return null;
		} else {
			$tanggal = explode('-',$tanggal);
			switch ($tanggal[1]) {

				case '01':
					return "Januari";
					break;

				case '02':
					return "Februari";
					break;

				case '03':
					return "Maret";
					break;
				
				case '04':
					return "April";
					break;

				case '05':
					return "Mei";
					break;

				case '06':
					return "Juni";
					break;

				case '07':
					return "Juli";
					break;

				case '08':
					return "Agustus";
					break;

				case '09':
					return "September";
					break;

				case '10':
					return "Oktober";
					break;

				case '11':
					return "November";
					break;

				case '12':
					return "Desember";
					break;
			}
		}

	}

	public static function getTerbilang($rp,$tri)
	{
		$ones = array(
			"",
			" satu",
			" dua",
			" tiga",
			" empat",
			" lima",
			" enam",
			" tujuh",
			" delapan",
			" sembilan",
			" sepuluh",
			" sebelas",
			" dua belas",
			" tiga belas",
			" empat belas",
			" lima belas",
			" enam belas",
			" tujuh belas",
			" delapan belas",
			" sembilan belas"
		);

		$tens = array(
			"",
			"",
			" dua puluh",
			" tiga puluh",
			" empat puluh",
			" lima puluh",
			" enam puluh",
			" tujuh puluh",
			" delapan puluh",
			" sembilan puluh"
		);

		$triplets = array(
			"",
			" ribu",
			" juta",
			" miliar",
			" triliun",
		);

		// chunk the number, ...rxyy
		$r = (int) ($rp / 1000);
		$x = ($rp / 100) % 10;
		$y = $rp % 100;

		// init the output string
		$str = "";

		// do hundreds
		if ($x > 0)
		{
			if($x==1)	
				$str =  "seratus";
			else
				$str = $ones[$x] . " ratus";
		}
		
		// do ones and tens
		if ($y < 20)
			$str .= $ones[$y];
		else
			$str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

		// add triplet modifier only if there
		// is some output to be modified...
		if ($str != "")
			$str .= $triplets[$tri];

		// continue recursing?
		if ($r > 0)
			return Helper::getTerbilang($r, $tri+1).$str;
		else
			return $str;
	}	

}

?>