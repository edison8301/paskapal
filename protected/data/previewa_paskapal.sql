-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2016 at 05:46 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `previewa_paskapal`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan_utama`
--

CREATE TABLE `bahan_utama` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan_utama`
--

INSERT INTO `bahan_utama` (`id`, `nama`) VALUES
(1, 'Kayu'),
(2, 'Fiber'),
(3, 'Besi'),
(4, 'Ferocement');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kapal`
--

CREATE TABLE `jenis_kapal` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kapal`
--

INSERT INTO `jenis_kapal` (`id`, `nama`) VALUES
(1, 'Penangkap Ikan'),
(2, 'Pengangukut Ikan'),
(3, 'Survey'),
(4, 'Wisata');

-- --------------------------------------------------------

--
-- Table structure for table `kapal`
--

CREATE TABLE `kapal` (
  `id` int(11) NOT NULL,
  `nama_kapal` varchar(200) NOT NULL,
  `tanda_pas` varchar(200) DEFAULT NULL,
  `nama_pemilik` varchar(255) DEFAULT NULL,
  `alamat_pemilik` varchar(255) DEFAULT NULL,
  `npwp_pemilik` varchar(255) DEFAULT NULL,
  `ukuran_p` decimal(5,2) DEFAULT NULL,
  `ukuran_l` decimal(5,2) DEFAULT NULL,
  `ukuran_d` decimal(5,2) DEFAULT NULL,
  `tonase_kotor` decimal(5,2) DEFAULT NULL,
  `tonase_bersih` decimal(5,2) DEFAULT NULL,
  `tempat_pembangunan` varchar(255) DEFAULT NULL,
  `tahun_pembangunan` year(4) DEFAULT NULL,
  `bahan_utama` varchar(200) DEFAULT NULL,
  `jumlah_geladak` int(11) DEFAULT NULL,
  `penggerak` varchar(200) DEFAULT NULL,
  `mesin_merek` varchar(200) DEFAULT NULL,
  `mesin_daya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kapal`
--

INSERT INTO `kapal` (`id`, `nama_kapal`, `tanda_pas`, `nama_pemilik`, `alamat_pemilik`, `npwp_pemilik`, `ukuran_p`, `ukuran_l`, `ukuran_d`, `tonase_kotor`, `tonase_bersih`, `tempat_pembangunan`, `tahun_pembangunan`, `bahan_utama`, `jumlah_geladak`, `penggerak`, `mesin_merek`, `mesin_daya`) VALUES
(105, 'Montana', 'A14 NO 105', 'Alexander', 'Belitung Timur', '100 567 20003 40001', '1.50', '23.50', '4.50', '9.00', '10.00', 'United States', 2005, 'KAYU', 4, 'ANGIN', 'MANUAL', 'TENAGA ANGIN'),
(106, 'Yamato', 'B90 NO 89', 'Fuwa', 'Belitung Timur 2', '270 907 20003 40005', '5.00', '59.00', '9.00', '20.00', '16.00', 'Jepang', 2011, 'Besi', 8, 'Mesin', 'Manual', 'Batu Bara');

-- --------------------------------------------------------

--
-- Table structure for table `pas_kecil`
--

CREATE TABLE `pas_kecil` (
  `id` int(11) NOT NULL,
  `id_kapal` int(11) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `nama_kapal` varchar(255) DEFAULT NULL,
  `nama_pemilik` varchar(255) DEFAULT NULL,
  `alamat_pemilik` varchar(255) DEFAULT NULL,
  `tanda_pas` varchar(255) DEFAULT NULL,
  `gross_tonnage` varchar(255) DEFAULT NULL,
  `net_tonnage` varchar(255) DEFAULT NULL,
  `panjang_kapal` varchar(20) DEFAULT NULL,
  `lebar_kapal` varchar(20) DEFAULT NULL,
  `dalam_kapal` varchar(20) DEFAULT NULL,
  `mesin_penggerak` varchar(255) DEFAULT NULL,
  `merk_mesin` varchar(255) DEFAULT NULL,
  `daya_mesin` varchar(255) DEFAULT NULL,
  `bahan_utama` int(11) DEFAULT NULL,
  `jumlah_geladak` int(11) NOT NULL,
  `jumlah_baling_baling` int(11) DEFAULT NULL,
  `jenis_kapal` int(11) DEFAULT NULL,
  `tempat_pembuatan_kapal` varchar(255) DEFAULT NULL,
  `tahun_pembuatan_kapal` int(4) DEFAULT NULL,
  `tempat_register_kapal` varchar(255) DEFAULT NULL,
  `nomor_register_kapal` varchar(255) DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `tujuan_penggunaan` varchar(255) DEFAULT NULL,
  `tanggal_pemberian` date DEFAULT NULL,
  `tempat_pemberian` varchar(255) DEFAULT NULL,
  `tanggal_batas_berlaku` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pas_kecil`
--

INSERT INTO `pas_kecil` (`id`, `id_kapal`, `nomor`, `nama_kapal`, `nama_pemilik`, `alamat_pemilik`, `tanda_pas`, `gross_tonnage`, `net_tonnage`, `panjang_kapal`, `lebar_kapal`, `dalam_kapal`, `mesin_penggerak`, `merk_mesin`, `daya_mesin`, `bahan_utama`, `jumlah_geladak`, `jumlah_baling_baling`, `jenis_kapal`, `tempat_pembuatan_kapal`, `tahun_pembuatan_kapal`, `tempat_register_kapal`, `nomor_register_kapal`, `catatan`, `tujuan_penggunaan`, `tanggal_pemberian`, `tempat_pemberian`, `tanggal_batas_berlaku`) VALUES
(2, 105, 'AL.801/1/8', 'Nama Kapal 123', 'Nama Pemilik234', 'Alamat Pemilik 1', 'Tanda Pas / 859', '2', '0', '9', '5', '4', 'Motor', '', '', 3, 0, 0, 3, '', 2009, 'Belitung Selatan', 'No. Reg. 123', 'Catatan-catatan', 'Berlayar', '2016-06-16', 'Belitung Utara', '2016-06-29'),
(3, 106, 'AL.801/1/6', NULL, NULL, NULL, '', '', '0', '', '', '', '', '', '', 0, 0, 0, 0, '', 0, '', '', '', 'Pulau Belitung', '2016-10-26', 'Russia', '2016-12-01'),
(6, NULL, '233', 'nama123', 'pemilik', 'alamat', 'tanda pas ', '5', '2', '12', '6', '5', 'Angin', 'Merk Mesin 123', 'Daya Mesin 9887', 1, 4, 2, 4, 'Belitung Pusat', 1999, 'Belitung Barat', 'No. Reg 789', 'Catatan - catatan', 'wisata', '2016-06-15', 'Belitung Timur', '2016-06-24');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nilai` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `kode`, `nilai`) VALUES
(1, 'penandatangan_nama', 'Penandatangan Contoh'),
(2, 'penandatangan_nip', '1008313712387123'),
(3, 'retribusi_kode_rekening', '4.1.2.01.07');

-- --------------------------------------------------------

--
-- Table structure for table `retribusi`
--

CREATE TABLE `retribusi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_urut` int(11) DEFAULT NULL,
  `alamat` text,
  `npwr` varchar(255) DEFAULT NULL,
  `tanggal_jatuh_tempo` date DEFAULT NULL,
  `tanggal_retribusi` date DEFAULT NULL,
  `masa_berlaku_uji` date DEFAULT NULL,
  `sampai_dengan_tanggal` date DEFAULT NULL,
  `uraian_retribusi` varchar(255) DEFAULT NULL,
  `jumlah_ketetapan` varchar(20) NOT NULL,
  `jumlah_sanksi_bunga` varchar(20) DEFAULT NULL,
  `jumlah_sanksi_kenaikan` varchar(20) DEFAULT NULL,
  `jumlah_keseluruhan` varchar(20) DEFAULT NULL,
  `gross_tonnage` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retribusi`
--

INSERT INTO `retribusi` (`id`, `nama`, `no_urut`, `alamat`, `npwr`, `tanggal_jatuh_tempo`, `tanggal_retribusi`, `masa_berlaku_uji`, `sampai_dengan_tanggal`, `uraian_retribusi`, `jumlah_ketetapan`, `jumlah_sanksi_bunga`, `jumlah_sanksi_kenaikan`, `jumlah_keseluruhan`, `gross_tonnage`) VALUES
(1, 'Hamzah Ahmadds', 2, 'Jln. Melati Rt. 36/-11 DS. LENGGANG A 13 No.755', 'npwr123123', '2017-10-31', '2016-06-21', '2016-05-03', '2016-05-31', 'Retribusi Pengujian Kapal Ukuran GT 3 s.d GT4', '90000', '0', '0', '30000', '1'),
(2, 'Iqbal Hamsyah', 3, 'Jln. Cibaduyut Raya No. 38', 'Npwr 234', '2016-05-31', '2016-06-20', '2016-05-12', '2016-06-30', 'Retribusi Pengujian Kapal Ukuran GT 1 s.d GT2', '30000', '0', '0', '30000', '4'),
(3, 'Iqbal', 9, '123', '123123', '2016-06-19', '2016-06-27', '2016-06-20', '2016-06-21', 'Retribusi Pengujian Kapal Ukuran GT 1 s.d GT2', '30000', '1231', '12312', '123123', '12');

-- --------------------------------------------------------

--
-- Table structure for table `retribusi_jumlah_ketetapan`
--

CREATE TABLE `retribusi_jumlah_ketetapan` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nilai` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retribusi_jumlah_ketetapan`
--

INSERT INTO `retribusi_jumlah_ketetapan` (`id`, `kode`, `nilai`) VALUES
(1, 'ketetapan1', '30000'),
(2, 'ketetapan2', '90000'),
(3, 'ketetapan3', '120000');

-- --------------------------------------------------------

--
-- Table structure for table `retribusi_uraian`
--

CREATE TABLE `retribusi_uraian` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nilai` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retribusi_uraian`
--

INSERT INTO `retribusi_uraian` (`id`, `kode`, `nilai`) VALUES
(1, 'uraian 1', 'Retribusi Pengujian Kapal Ukuran GT 1 s.d GT2'),
(2, 'uraian 2', 'Retribusi Pengujian Kapal Ukuran GT 3 s.d GT4'),
(3, 'uraian3', 'Retribusi Pengujian Kapal Ukuran GT 5 s.d GT7');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'operator');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `id_role`) VALUES
(1, 'admin', '$2y$13$2UOhRMvwekQQERNL3T7oq.WZ6eHqmtzXgypl/qlDXfl43g5wEtQbW', 1),
(2, 'operator', '$2y$13$PaE54WMAcm7BtPBAjVciMO4ostoxbqVqdT68jfdAQJb/QFQc0bA.S', 2),
(4, 'tes', '$2y$13$pAlarRxxu.9PDIwPyL3nl.kP8VOUX3mxd//LCNEfIRRaWsxVZDPEq', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan_utama`
--
ALTER TABLE `bahan_utama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_kapal`
--
ALTER TABLE `jenis_kapal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kapal`
--
ALTER TABLE `kapal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pas_kecil`
--
ALTER TABLE `pas_kecil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retribusi`
--
ALTER TABLE `retribusi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retribusi_jumlah_ketetapan`
--
ALTER TABLE `retribusi_jumlah_ketetapan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retribusi_uraian`
--
ALTER TABLE `retribusi_uraian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan_utama`
--
ALTER TABLE `bahan_utama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jenis_kapal`
--
ALTER TABLE `jenis_kapal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kapal`
--
ALTER TABLE `kapal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `pas_kecil`
--
ALTER TABLE `pas_kecil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `retribusi`
--
ALTER TABLE `retribusi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `retribusi_jumlah_ketetapan`
--
ALTER TABLE `retribusi_jumlah_ketetapan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `retribusi_uraian`
--
ALTER TABLE `retribusi_uraian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
